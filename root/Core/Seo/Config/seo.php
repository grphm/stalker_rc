<?php
return [
    'package_name' => 'core_seo',
    'package_title' => ['ru' => 'Модуль SEO', 'en' => 'SEO module', 'es' => 'Módulo de SEO'],
    'package_icon' => 'zmdi zmdi-globe',
    'relations' => [],
    'package_description' => [
        'ru' => 'Работа с поисковой оптимизацией',
        'en' => 'Dealing with search engine optimization',
        'es' => 'Se trata de la optimización de motores de búsqueda'
    ],
    'version' => [
        'ver' => 1.0,
        'date' => '15.04.2016'
    ]
];
