<?php
namespace STALKER_CMS\Core\Content\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Фасад контроллера страница
 * Class PublicPage
 * @package STALKER_CMS\Core\Content\Facades
 */
class PublicPage extends Facade {

    protected static function getFacadeAccessor() {

        return 'PublicPagesController';
    }
}