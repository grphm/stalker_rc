<?php
namespace STALKER_CMS\Core\Content\Http\Controllers;

use STALKER_CMS\Vendor\Http\Controllers\Controller;

/**
 * Основной контроллер пакета
 * Class ModuleController
 * @package STALKER_CMS\Core\Content\Http\Controllers
 */
abstract class ModuleController extends Controller {

    function __construct() {

        $this->locale_prefix = (\App::getLocale() == settings(['core_system', 'settings', 'base_locale'])) ? '' : \App::getLocale().'/';
        view()->share('locale_prefix', $this->locale_prefix);
    }
}