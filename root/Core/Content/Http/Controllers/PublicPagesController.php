<?php
namespace STALKER_CMS\Core\Content\Http\Controllers;

use STALKER_CMS\Core\Content\Models\Page;
use STALKER_CMS\Core\Content\Models\PageBlock;
use STALKER_CMS\Core\Content\Models\PageTemplate;

/**
 * Контроллер для получения страниц в гостевом интерфейсе
 * Class PublicPagesController
 * @package STALKER_CMS\Core\Content\Http\Controllers
 */
class PublicPagesController extends ModuleController {

    /**
     * Загрузить данные страницы в шаблон
     * и отобразить на экране
     * @param Page $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showPage(Page $page) {

        if(settings(['core_system', 'settings', 'services_mode'])):
            if(view()->exists("home_views::errors.1503")):
                return view("home_views::errors.1503", ['code' => 1503, 'message' => trans('root_lang::codes.1503')]);
            elseif(view()->exists("root_views::errors.1503")):
                return view("root_views::errors.1503", ['code' => 1503, 'message' => trans('root_lang::codes.1503')]);
            endif;
        endif;
        $blocks = PageBlock::wherePageId($page->id)->with('template')->lists('content', 'slug');
        $template = PageTemplate::findOrFail($page->template_id);
        $view_path = $this->getViewPath($template);
        if(view()->exists("home_views::$view_path")):
            return view("home_views::$view_path", compact('page', 'blocks'));
        else:
            abort(404);
        endif;
    }

    /**
     * Создать список страниц по заданным условиям
     * @param array $attributes
     * @return mixed
     */
    function getPagesList(array $attributes = []) {

        $locale = $this->getLocale();
        if(empty($attributes)):
            return Page::whereLocale($locale)->wherePublication(TRUE)->get();
        else:
            return Page::where($attributes)->get();
        endif;
    }

    /**
     * Вернуть локаль в зависимости
     * от первого сегмента URL
     * @return null
     */
    public function setLocale() {

        $locale = $this->getLocale();
        \App::setLocale($locale);
    }

    /**
     * Вернуть локаль в зависимости
     * от первого сегмента URL
     * @return null
     */
    public function getLocale() {

        $first_uri_segment = \Request::segment(1);
        $locales = \App::make('Locales');
        if(isset($locales[$first_uri_segment])):
            return $locales[$first_uri_segment];
        endif;
        return config('app.locale');
    }

    /**
     * Вернуть префикс локали в зависимости
     * от первого сегмента URL
     * @return null
     */
    public function localePrefix() {

        $first_uri_segment = \Request::segment(1);
        $locale_prefix = NULL;
        if(is_null($first_uri_segment) || $first_uri_segment == '/'):
            $first_uri_segment = config('app.locale');
        endif;
        $locales = \App::make('Locales');
        if(isset($locales[$first_uri_segment])):
            $locale_prefix = $locales[$first_uri_segment];
        endif;
        return $locale_prefix;
    }

    /**
     * Изменить локаль и вернуть ссылку на страницу
     * или на корень сайта
     * @param $locale
     * @param null $page
     * @return \Illuminate\Contracts\Routing\UrlGenerator|mixed|string
     */
    public function changeLocale($locale, $page = NULL) {

        $locales = \App::make('Locales');
        if(isset($locales[$locale])):
            \App::setLocale($locale);
        else:
            \App::setLocale(config('app.locale'));
        endif;
        if(!is_null($page) && $page instanceof Page):
            return $page->PageUrl;
        else:
            return url($locales[$locale]);
        endif;
    }

    /**
     * Вернуть путь в файлу шаблона
     * @param $template
     * @return string
     */
    public function getViewPath($template) {

        $locale_prefix = ($template->locale == settings(['core_system', 'settings', 'base_locale'])) ? '' : $template->locale.'.';
        return substr(remove_first_slash($locale_prefix.$template->path), 0, -10);
    }
}