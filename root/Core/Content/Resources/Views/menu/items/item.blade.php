<div class="pull-left">
    <i class="zmdi zmdi-swap-vertical f-20 p-t-10 cursor-move"></i>
</div>
<div class="pull-right">
    <div class="actions dropdown">
        <a aria-expanded="true" data-toggle="dropdown" href="">
            <i class="zmdi zmdi-more-vert"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            <li>
                <a href="{!! route('core.content.menu.items_edit', [$menu->id, $element['id']]) !!}">
                    @lang('core_content_lang::menu_items.edit')
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <a class="c-red js-item-remove" href="">
                    @lang('core_content_lang::menu_items.delete.submit')
                </a>
                {!! Form::open(['route' => ['core.content.menu.items_destroy', $menu->id, $element['id']], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                <button type="submit"
                        data-question="@lang('core_content_lang::menu_items.delete.question') &laquo;{{ $element['title'] }}&raquo;?"
                        data-confirmbuttontext="@lang('core_content_lang::menu_items.delete.confirmbuttontext')"
                        data-cancelbuttontext="@lang('core_content_lang::menu_items.delete.cancelbuttontext')">
                </button>
                {!! Form::close() !!}
            </li>
        </ul>
    </div>
</div>
<div class="media-body">
    <div class="lgi-heading">{!! $element['title'] !!}</div>
    <small class="lgi-text">{!! $types[$element['item_type']] or '' !!}</small>
    @if(in_array($element['item_type'], ['page_link', 'anchor_link']) && empty($element['page']))
        <span class="badge bgm-red c-white f-10 p-3">@lang('core_content_lang::menu_items.page_missing')</span>
    @elseif($element['item_type'] == 'file_link' && empty($element['file']))
        <span class="badge bgm-red c-white f-10 p-3">@lang('core_content_lang::menu_items.file_missing')</span>
    @endif
</div>