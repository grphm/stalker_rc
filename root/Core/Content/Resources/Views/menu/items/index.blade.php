@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_content::menu.icon') }}"></i> {!! array_translate(config('core_content::menu.title')) !!}
        </li>
        <li>
            <a href="{!! route('core.content.menu.index') !!}">
                <i class="{{ config('core_content::menu.menu_child.menu.icon') }}"></i> {!! array_translate(config('core_content::menu.menu_child.menu.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-receipt"></i> @lang('core_content_lang::menu_items.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2><i class="zmdi zmdi-receipt"></i> @lang('core_content_lang::menu_items.breadcrumb')</h2>
    </div>
    @BtnAdd('core.content.menu.items_create', $menu->id)
    <div class="card">
        <div class="list-group">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">{!! $menu->title !!}</div>
            </div>
            <div class="card-body card-padding m-h-250 p-l-10">
                <div class="list-group js-nestable-list">
                    @if($menu->items->count())
                        {!! $list !!}
                    @else
                        <h2 class="f-16 c-gray m-l-20">@lang('core_content_lang::menu.empty')</h2>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts_before')
    <script>
        $(".js-nestable-list ul").eq(0).removeClass('p-l-20').addClass('menu-nestable-list');
        var nestableList = [];
        var list = $('.menu-nestable-list').sortable({
            group: 'menu-nestable-list',
            delay: 500,
            handle: 'i.zmdi-swap-vertical',
            onDrop: function ($item, container, _super) {
                var serialize = list.sortable("serialize").get();
                $.ajax({
                    url: '{!! route('core.content.menu.sortable', $menu->id) !!}',
                    type: 'POST',
                    dataType: 'json',
                    data: {elements: JSON.stringify(serialize[0], null, '')},
                    beforeSend: function () {
                    },
                    success: function (response, textStatus, xhr) {
                        if (response.status == true) {
                            BASIC.notify(null, response.responseText, 'bottom', 'center', 'zmdi zmdi-notifications-none zmdi-hc-fw', 'success', 'animated flipInX', 'animated flipOutX');
                        } else {
                            BASIC.notify(null, response.errorText, 'bottom', 'center', null, 'danger', 'animated flipInX', 'animated flipOutX');
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        BASIC.notify(null, xhr.responseText, 'bottom', 'center', null, 'danger', 'animated flipInX', 'animated flipOutX');
                    }
                });
                _super($item, container);
            }
        });
    </script>
@stop