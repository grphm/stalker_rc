@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_content::menu.icon') }}"></i> {!! array_translate(config('core_content::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('core_content::menu.menu_child.menu.icon') }}"></i> {!! array_translate(config('core_content::menu.menu_child.menu.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_content::menu.menu_child.menu.icon') }}"></i>
            {!! array_translate(config('core_content::menu.menu_child.menu.title')) !!}
        </h2>
    </div>
    @BtnAdd('core.content.menu.create')
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="card-body card-padding m-h-250 p-0">
                <div class="action-header clearfix">
                    <div class="ah-label hidden-xs">
                        @lang('core_content_lang::menu.list')
                    </div>
                </div>
                <div class="card-body card-padding m-h-250 p-0">
                    @forelse($menus as $index => $menu)
                        <div class="js-item-container list-group-item media">
                            <div class="pull-right">
                                <div class="actions dropdown">
                                    <a aria-expanded="true" data-toggle="dropdown" href="">
                                        <i class="zmdi zmdi-more-vert"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        @if(\PermissionsController::allowPermission('core_content', 'edit', FALSE))
                                            <li>
                                                <a href="{!! route('core.content.menu.items_index', $menu->id) !!}">
                                                    @lang('core_content_lang::menu.items')
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{!! route('core.content.menu.edit', $menu->id) !!}">
                                                    @lang('core_content_lang::menu.edit')
                                                </a>
                                            </li>
                                        @endif
                                        <li>
                                            <a href="javascript:void(0);" class="js-copy-link" data-clipboard-text="{!! '@'.'Menu(\'' . $menu->slug . '\')' !!}">
                                                @lang('core_content_lang::menu.embed')
                                            </a>
                                        </li>
                                        @if(\PermissionsController::allowPermission('core_content', 'delete', FALSE))
                                            <li class="divider"></li>
                                            <li>
                                                <a class="c-red js-item-remove" href="">
                                                    @lang('core_content_lang::menu.delete.submit')
                                                </a>
                                                {!! Form::open(['route' => ['core.content.menu.destroy', $menu->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                                <button type="submit"
                                                        data-question="@lang('core_content_lang::menu.delete.question') &laquo;{{ $menu->title }}&raquo;?"
                                                        data-confirmbuttontext="@lang('core_content_lang::menu.delete.confirmbuttontext')"
                                                        data-cancelbuttontext="@lang('core_content_lang::menu.delete.cancelbuttontext')">
                                                </button>
                                                {!! Form::close() !!}
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            <div class="media-body">
                                <div class="lgi-heading">{!! $menu->title !!}</div>
                                @if($menu->template)
                                    <small class="lgi-text">
                                        {!! double_slash('/home/Resources/Views/'.$locale_prefix. '/' . $menu->template->path) !!}
                                    </small>
                                @endif
                                <ul class="lgi-attrs">
                                    <li>ID: @numDimensions($menu->id)</li>
                                    <li>
                                        @lang('core_content_lang::menu.symbolic_code'):
                                        {!! $menu->slug !!}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    @empty
                        <h2 class="f-16 c-gray m-l-30">@lang('core_content_lang::menu.empty')</h2>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
@stop