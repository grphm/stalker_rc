@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_content::menu.icon') }}"></i> {!! array_translate(config('core_content::menu.title')) !!}
        </li>
        <li>
            <a href="{!! route('core.content.pages.index') !!}">
                <i class="{{ config('core_content::menu.menu_child.pages.icon') }}"></i> {!! array_translate(config('core_content::menu.menu_child.pages.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-view-subtitles"></i> @lang('core_content_lang::pages.blocks')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2><i class="zmdi zmdi-view-subtitles"></i> @lang('core_content_lang::pages.blocks')</h2>
    </div>
    <a href="{!! route('core.content.pages.blocks_create', $page->id) !!}"
       class="btn bgm-deeppurple m-btn btn-float waves-effect waves-circle waves-float">
        <i class="zmdi zmdi-plus"></i>
    </a>
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">{{ $page->title }}</div>
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @forelse($blocks as $block)
                    <div class="js-item-container list-group-item media">
                        <div class="pull-right">
                            <div class="actions dropdown">
                                <a aria-expanded="true" data-toggle="dropdown" href="">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="{!! route('core.content.pages.blocks_edit', [$page->id, $block->id]) !!}">
                                            @lang('core_content_lang::blocks.edit')
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" class="js-copy-link"
                                           data-clipboard-text="{!! '@'.'PageBlock(\'' . $block->slug . '\')' !!}">
                                            @lang('core_content_lang::blocks.embed')
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a class="c-red js-item-remove" href="">
                                            @lang('core_content_lang::blocks.delete.submit')
                                        </a>
                                        {!! Form::open(['route' => ['core.content.pages.blocks_destroy', $page->id, $block->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                        <button type="submit"
                                                data-question="@lang('core_content_lang::blocks.delete.question') &laquo;{{ $block->slug }}&raquo;?"
                                                data-confirmbuttontext="@lang('core_content_lang::blocks.delete.confirmbuttontext')"
                                                data-cancelbuttontext="@lang('core_content_lang::blocks.delete.cancelbuttontext')">
                                        </button>
                                        {!! Form::close() !!}
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="lgi-heading">{!! $block->title !!}</div>
                            <small class="lgi-text">
                                {!! \Illuminate\Support\Str::limit(strip_tags($block->content), 500) !!}
                            </small>
                            <ul class="lgi-attrs">
                                <li>
                                    @lang('core_content_lang::blocks.insert.form.slug'): {!! $block->slug !!}
                                </li>
                            </ul>
                        </div>
                    </div>
                @empty
                    <h2 class="f-16 c-gray m-l-30">@lang('core_content_lang::blocks.empty')</h2>
                @endforelse
            </div>
        </div>
    </div>
@stop