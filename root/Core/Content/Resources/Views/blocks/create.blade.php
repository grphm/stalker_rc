@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_content::menu.icon') }}"></i> {!! array_translate(config('core_content::menu.title')) !!}
        </li>
        <li>
            <a href="{!! route('core.content.pages.index') !!}">
                <i class="{{ config('core_content::menu.menu_child.pages.icon') }}"></i> {!! array_translate(config('core_content::menu.menu_child.pages.title')) !!}
            </a>
        </li>
        <li>
            <a href="{!! route('core.content.pages.blocks_index', $page->id) !!}">
                <i class="zmdi zmdi-view-subtitles"></i> @lang('core_content_lang::pages.blocks')
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-plus"></i> @lang('core_content_lang::blocks.insert.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2><i class="zmdi zmdi-plus"></i> @lang('core_content_lang::blocks.insert.title')</h2>
    </div>
    <div class="card">
        <div class="action-header clearfix">
            <div class="ah-label hidden-xs">{!! $page->title !!}</div>
        </div>
        <div class="card-body card-padding">
            <div class="row">
                {!! Form::open(['route' => ['core.content.pages.blocks_store', $page->id], 'class' => 'form-validate', 'id' => 'add-content-block-form']) !!}
                {!! Form::hidden('page_id', $page->id) !!}
                <div class="col-sm-9">
                    <div class="form-group">
                        <p class="c-gray m-b-20">@lang('core_content_lang::blocks.insert.form.content')</p>
                        {!! Form::textarea('content', NULL, ['id' => 'block_content']) !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                            <label class="fg-label">@lang('core_content_lang::blocks.insert.form.title')</label>
                        </div>
                    </div>
                    <div class="form-group input-group fg-float input-group-help-block">
                        <div class="fg-line p-0 l-0 w-full">
                            {!! Form::text('slug', NULL, ['class' => 'input-sm form-control fg-input', 'autocomplete' => 'off']) !!}
                            <label class="fg-label">@lang('core_content_lang::blocks.insert.form.slug')</label>
                        </div>
                        <span class="input-group-addon last bgm-white">
                            <button type="button" id="js-generate" class="btn btn-primary btn-icon waves-effect waves-circle waves-float">
                                <i class="zmdi zmdi-flash f-16"></i>
                            </button>
                        </span>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                                <i class="fa fa-save"></i>
                                <span class="btn-text">@lang('core_content_lang::blocks.insert.form.submit')</span>
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts_after')
    @set($summernote_locale, \App::getLocale() . '-' . strtoupper(\App::getLocale()))
    {!! Html::script('core/summernote/summernote-' . $summernote_locale . '.js') !!}
    <script>
        $("#block_content").summernote({
            height: 250,
            tabsize: 2,
            lang: '{{ $summernote_locale }}',
            toolbar: [
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture']],
                ['view', ['fullscreen', 'codeview']],
            ]
        });
        $('#block_content').on('summernote.change', function (we, contents, $editable) {
            $("#block_content").html(contents);
            $("#block_content").change();
        });
        $("#js-generate").click(function () {
            $("input[name='slug']").str_random();
            $("input[name='slug']").parent().addClass('fg-toggled');
            $("input[name='slug']").parents('.input-group-help-block').removeClass('has-error').addClass('has-success').find('.help-block').remove();
        });
    </script>
@stop