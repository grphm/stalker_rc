@extends('home_views::layout')
@metaTitle()
@metaDescription()
@section('content')
    <h1>@lang("home_lang::main.index.title")</h1>
    <p>@lang("home_lang::main.index.description")</p>
    <p>@lang("home_lang::main.contacts.description")</p>
    <ul>
        <li>@lang("home_lang::main.contacts.phone")</li>
        <li>@lang("home_lang::main.contacts.email")</li>
    </ul>
@stop