<?php
return [
    'search' => 'Enter the name of the item list',
    'structure_empty' => 'The structure of the dictionary is empty',
    'edit' => 'Edit',
    'delete' => [
        'question' => 'Delete dictionary item',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete'
    ],
    'empty' => 'List is empty',
    'module_disabled' => 'Module is not active',
    'variable_not_set' => 'The value is not defined',
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding a dictionary item',
        'form' => [
            'title' => 'Title',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Editing the dictionary item',
        'form' => [
            'title' => 'Title',
            'submit' => 'Save'
        ]
    ]
];