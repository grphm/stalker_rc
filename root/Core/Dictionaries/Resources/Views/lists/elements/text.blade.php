@if(isset($element['name']))
    <div class="form-group fg-float">
        <div class="fg-line">
            {!! Form::text('fields['.$element['name'].']', isset($element['value']) ? $element['value'] : NULL, ['class'=>'input-sm form-control fg-input']) !!}
            <label class="fg-label">{!! $element['placeholder'] !!}</label>
        </div>
    </div>
@else
    <div class="m-t-15 clearfix">
        <mark>
            {!! $element['placeholder'] !!}. @lang('core_dictionaries_lang::lists.variable_not_set')
        </mark>
    </div>
@endif