@if(isset($element['name']))
    <p class="f-100 c-gray m-b-20">{!! $element['placeholder'] !!}</p>
    @if(isset($element['value']) && !empty($element['value']))
        <div class="clearfix m-b-10">
            <a target="_blank" href="{!! asset('uploads'.$element['value']) !!}">
                {!! basename($element['value']) !!}
            </a>
        </div>
    @endif
    <div class="fileinput fileinput-new" data-provides="fileinput">
        <span class="btn btn-primary btn-file m-r-10 waves-effect">
            <span class="fileinput-new">
                @if(isset($element['value']) && !empty($element['value']))
                    @lang('core_dictionaries_lang::elements.file.file_change')
                @else
                    @lang('core_dictionaries_lang::elements.file.file_select')
                @endif
            </span>
            <span class="fileinput-exists">@lang('core_dictionaries_lang::elements.file.file_change')</span>
            {!! Form::file($element['name']) !!}
        </span>
        <span class="fileinput-filename"></span>
        <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
    </div>
@else
    <div class="m-t-15 clearfix">
        <mark>
            {!! $element['placeholder'] !!}. @lang('core_dictionaries_lang::lists.variable_not_set')
        </mark>
    </div>
@endif