@if(isset($element['name']))
    <?php
    $pages = []; $module_enabled = FALSE;
    if(\PermissionsController::isPackageEnabled('core_content')):
        $pages[''] = array_translate(['ru' => 'Не указано', 'en' => 'Not indicated', 'es' => 'No especificado']);
        foreach(\STALKER_CMS\Core\Content\Models\Page::whereLocale(\App::getLocale())->wherePublication(TRUE)->lists('title', 'slug') as $slug => $title):
            $pages[$slug] = $title;
        endforeach;
        $module_enabled = TRUE;
    endif;
    ?>
    @if($module_enabled)
        <div class="form-group">
            <p class="f-500 c-black m-b-15">{!! $element['placeholder'] !!}</p>
            {!! Form::select('fields['.$element['name'].']', $pages, isset($element['value']) ? $element['value'] : NULL, ['class' => 'tag-select']) !!}
        </div>
    @else
        <div class="m-t-15 clearfix">
            <mark>@lang('core_dictionaries_lang::lists.module_disabled')</mark>
        </div>
    @endif
@else
    <div class="m-t-15 clearfix">
        <mark>
            {!! $element['placeholder'] !!}. @lang('core_dictionaries_lang::lists.variable_not_set')
        </mark>
    </div>
@endif