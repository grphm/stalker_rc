@if(isset($element['name']))
    @if(\STALKER_CMS\Core\Dictionaries\Models\Dictionary::whereSlug($element['data_item'])->exists())
        <?php
        try {
            $dictionary = \STALKER_CMS\Core\Dictionaries\Models\Dictionary::whereSlug($element['data_item'])->first()->lists()->pluck('title', 'id');
        } catch(\Exception $e) {
            $dictionary = [];
        }
        ?>
        <div class="form-group">
            <p class="f-500 c-black m-b-15">{!! $element['placeholder'] !!}</p>
            @if(isset($element['multiple']) && $element['multiple'])
                {!! Form::select('fields['.$element['name'].'][]', $dictionary, isset($element['value']) ? explode(',', $element['value']) : NULL, ['class' => 'tag-select-multiple', 'multiple' => TRUE]) !!}
            @else
                {!! Form::select('fields['.$element['name'].']', $dictionary, isset($element['value']) ? $element['value'] : NULL, ['class' => 'tag-select']) !!}
            @endif
        </div>
    @elseif($element['data_item'] == 'core_countries')
        <?php $countries = \STALKER_CMS\Core\System\Models\Countries::whereLocale(\App::getLocale())->orderBy('title')->pluck('title', 'id'); ?>
        <div class="form-group">
            <p class="f-500 c-black m-b-15">{!! $element['placeholder'] !!}</p>
            @if(isset($element['multiple']) && $element['multiple'])
                {!! Form::select('fields['.$element['name'].'][]', $countries, isset($element['value']) ? explode(',', $element['value']) : NULL, ['class' => 'tag-select-multiple', 'multiple' => TRUE]) !!}
            @else
                {!! Form::select('fields['.$element['name'].']', $countries, isset($element['value']) ? $element['value'] : NULL, ['class' => 'tag-select']) !!}
            @endif
        </div>
    @elseif($element['data_item'] == 'core_cities')
        <?php $cities = \STALKER_CMS\Core\System\Models\Cities::whereLocale(\App::getLocale())->orderBy('title')->pluck('title', 'id'); ?>
        <div class="form-group">
            <p class="f-500 c-black m-b-15">{!! $element['placeholder'] !!}</p>
            @if(isset($element['multiple']) && $element['multiple'])
                {!! Form::select('fields['.$element['name'].'][]', $cities, isset($element['value']) ? explode(',', $element['value']) : NULL, ['class' => 'tag-select-multiple', 'multiple' => TRUE]) !!}
            @else
                {!! Form::select('fields['.$element['name'].']', $cities, isset($element['value']) ? $element['value'] : NULL, ['class' => 'tag-select']) !!}
            @endif
        </div>
    @elseif($element['data_item'] == 'core_galleries')
        @if(\PermissionsController::isPackageEnabled('core_galleries'))
            <?php $galleries = \STALKER_CMS\Core\Galleries\Models\Gallery::orderBy('title')->pluck('title', 'slug'); ?>
            <div class="form-group">
                <p class="f-500 c-black m-b-15">{!! $element['placeholder'] !!}</p>
                @if(isset($element['multiple']) && $element['multiple'])
                    {!! Form::select('fields['.$element['name'].'][]', $galleries, isset($element['value']) ? explode(',', $element['value']) : NULL, ['class' => 'tag-select-multiple', 'multiple' => TRUE]) !!}
                @else
                    {!! Form::select('fields['.$element['name'].']', $galleries, isset($element['value']) ? $element['value'] : NULL, ['class' => 'tag-select']) !!}
                @endif
            </div>
        @else
            <div class="m-t-15 clearfix">
                <mark>@lang('core_dictionaries_lang::lists.module_disabled')</mark>
            </div>
        @endif
    @elseif($element['data_item'] == 'core_uploads')
        @if(\PermissionsController::isPackageEnabled('core_uploads'))
            <?php $files = \STALKER_CMS\Core\Uploads\Models\Upload::orderBy('original_name')->pluck('original_name', 'id'); ?>
            <div class="form-group">
                <p class="f-500 c-black m-b-15">{!! $element['placeholder'] !!}</p>
                @if(isset($element['multiple']) && $element['multiple'])
                    {!! Form::select('fields['.$element['name'].'][]', $files, isset($element['value']) ? explode(',', $element['value']) : NULL, ['class' => 'tag-select-multiple', 'multiple' => TRUE]) !!}
                @else
                    {!! Form::select('fields['.$element['name'].']', $files, isset($element['value']) ? $element['value'] : NULL, ['class' => 'tag-select']) !!}
                @endif
            </div>
        @else
            <div class="m-t-15 clearfix">
                <mark>@lang('core_dictionaries_lang::lists.module_disabled')</mark>
            </div>
        @endif
    @endif
@else
    <div class="m-t-15 clearfix">
        <mark>
            {!! $element['placeholder'] !!}. @lang('core_dictionaries_lang::lists.variable_not_set')
        </mark>
    </div>
@endif