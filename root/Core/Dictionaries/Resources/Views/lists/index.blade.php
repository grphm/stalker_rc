@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li>
            <a href="{!! route('core.dictionaries.index') !!}">
                <i class="{{ config('core_dictionaries::menu.icon') }}"></i> {!! array_translate(config('core_dictionaries::menu.title')) !!}
            </a>
        </li>
        <li class="active">
            @lang('core_dictionaries_lang::dictionaries.lists')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>@lang('core_dictionaries_lang::dictionaries.lists')</h2>
    </div>
    @BtnAdd('core.dictionaries.lists_create', $dictionary->id)
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">
                    {!! $dictionary->title !!}
                </div>
                @if($lists->count())
                    {!! Form::open(['route' => ['core.dictionaries.lists_index', $dictionary->id], 'method' => 'get']) !!}
                    <div class="ah-search">
                        <input type="text" name="search" placeholder="@lang('core_dictionaries_lang::lists.search')" class="ahs-input">
                        <i class="ahs-close" data-ma-action="action-header-close">&times;</i>
                    </div>
                    {!! Form::close() !!}
                    <ul class="actions">
                        <li>
                            <a href="" data-ma-action="action-header-open">
                                <i class="zmdi zmdi-search"></i>
                            </a>
                        </li>
                    </ul>
                @endif
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @if($lists->count())
                    <ul class="nestable-list p-l-0" data-action-url="{!! route('core.dictionaries.lists_sortable', $dictionary->id) !!}">
                        @foreach($lists as $list)
                            <li class="list-group-item media js-item-container js-nestable-list-item" data-element="{!! $list->id !!}">
                                <div class="pull-left m-t-5">
                                    <i class="zmdi zmdi-swap-vertical f-20 p-t-10 cursor-move"></i>
                                </div>
                                <div class="pull-right">
                                    <div class="actions dropdown">
                                        <a aria-expanded="true" data-toggle="dropdown" href="">
                                            <i class="zmdi zmdi-more-vert"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li>
                                                <a href="{!! route('core.dictionaries.lists_edit', [$dictionary->id, $list->id]) !!}">
                                                    @lang('core_dictionaries_lang::lists.edit')
                                                </a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a class="c-red js-item-remove" href="">
                                                    @lang('core_dictionaries_lang::lists.delete.submit')
                                                </a>
                                                {!! Form::open(['route' => ['core.dictionaries.lists_destroy', $dictionary->id, $list->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                                <button type="submit"
                                                        data-question="@lang('core_dictionaries_lang::lists.delete.question') &laquo;{{ $list->title }}&raquo;?"
                                                        data-confirmbuttontext="@lang('core_dictionaries_lang::lists.delete.confirmbuttontext')"
                                                        data-cancelbuttontext="@lang('core_dictionaries_lang::lists.delete.cancelbuttontext')">
                                                </button>
                                                {!! Form::close() !!}
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <div class="lgi-heading">{!! $list->title !!}</div>
                                    <ul class="lgi-attrs">
                                        <li>ID: @numDimensions($list->id)</li>
                                    </ul>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                @else
                    <h2 class="f-16 c-gray m-l-30">@lang('core_dictionaries_lang::dictionaries.empty')</h2>
                @endif
            </div>
        </div>
    </div>
@stop