<?php
namespace STALKER_CMS\Core\Dictionaries\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use STALKER_CMS\Core\Dictionaries\Models\Dictionary;

/**
 * Контроллер для получения словарей в гостевом интерфейсе
 * Class PublicDictionariesController
 * @package STALKER_CMS\Core\Dictionaries\Http\Controllers
 */
class PublicDictionariesController extends ModuleController {

    /**
     * Получить список элементов словаря по символьному коду
     * @param $slug
     * @return Collection
     */
    public function lists($slug) {

        $lists = [];
        $dictionary = Dictionary::whereSlug($slug)->whereLocale(\App::getLocale())->with(['lists' => function($query) {

            $query->orderBy('order');
            $query->with('fields');
        }])->first();
        if($dictionary):
            foreach($dictionary->lists as $index => $list):
                $lists[$index] = [];
                foreach($list->fields as $field):
                    $lists[$index]['id'] = $list['id'];
                    $lists[$index][$field['field']] = $field['value'];
                endforeach;
            endforeach;
        endif;
        return collect($lists);
    }

    /**
     * Получить значения элемента словаря по символьному коду по ID элемента
     * @param $slug
     * @param $id
     * @return mixed|null
     */
    public function fields($slug, $id) {

        $items = $this->lists($slug);
        if($items->count()):
            foreach($items as $item):
                if($item['id'] == $id):
                    return $item;
                endif;
            endforeach;
        endif;
        return NULL;
    }

    /**
     * Постраничное представление элементов информационного списка
     * @param $slug
     * @param int $perPage
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function paginate($slug, $perPage = 10) {

        $elements = \Dictionary::lists($slug);
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPageSearchResults = $elements->slice(($currentPage - 1) * $perPage, $perPage)->all();
        return new LengthAwarePaginator($currentPageSearchResults, $elements->count(), $perPage);
    }
}