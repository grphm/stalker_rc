<?php
namespace STALKER_CMS\Core\Dictionaries\Http\Controllers;

use STALKER_CMS\Core\Dictionaries\Models\Dictionary;
use STALKER_CMS\Vendor\Http\Controllers\Controller;

/**
 * Основной контроллер пакета
 * Class ModuleController
 * @package STALKER_CMS\Core\Dictionaries\Http\Controllers
 */
abstract class ModuleController extends Controller {

    /**
     * Возвращает список доступных связей для элемента типа SELECT
     * @return array
     */
    public function getInformationItems() {

        $items = [];
        foreach(Dictionary::whereLocale(\App::getLocale())->lists('title', 'slug') as $slug => $title):
            $items[$slug] = $title;
        endforeach;
        $items['core_countries'] = trans('core_dictionaries_lang::dictionaries.select_data_items.countries');
        $items['core_cities'] = trans('core_dictionaries_lang::dictionaries.select_data_items.cities');
        if(\PermissionsController::isPackageEnabled('core_galleries')):
            $items['core_galleries'] = trans('core_dictionaries_lang::dictionaries.select_data_items.gallery');
        endif;
        if(\PermissionsController::isPackageEnabled('core_uploads')):
            $items['core_uploads'] = trans('core_dictionaries_lang::dictionaries.select_data_items.upload');
        endif;
        return $items;
    }
}