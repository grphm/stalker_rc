<?php
namespace STALKER_CMS\Core\Dictionaries\Http\Controllers;

use STALKER_CMS\Core\Dictionaries\Models\Dictionary;
use STALKER_CMS\Core\Dictionaries\Models\DictionaryLists;
use STALKER_CMS\Core\Dictionaries\Models\DictionaryListsFields;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

/**
 * Контроллер элемента словаря
 * Class DictionaryListsController
 * @package STALKER_CMS\Core\Dictionaries\Http\Controllers
 */
class DictionaryListsController extends ModuleController implements CrudInterface {

    protected $model;
    protected $dictionary;
    protected $fields;

    /**
     * DictionaryListsController constructor.
     * @param Dictionary $dictionary
     * @param DictionaryLists $lists
     * @param DictionaryListsFields $fields
     */
    public function __construct(Dictionary $dictionary, DictionaryLists $lists, DictionaryListsFields $fields) {

        $this->model = $lists;
        $this->dictionary = $dictionary->whereLocale(\App::getLocale())->where('id', \Request::segment(3))->first();
        $this->fields = $fields;
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        \PermissionsController::allowPermission('core_dictionaries', 'dictionaries');
        $request = \RequestController::init();
        $lists = $this->model->whereDictionaryId($this->dictionary->id)->orderBy('order');
        if($request::has('search')):
            $search = $request::input('search');
            $lists = $lists->where(function($query) use ($search) {

                $query->where('title', 'like', '%'.$search.'%');
            });
        endif;
        return view('core_dictionaries_views::lists.index', [
            'dictionary' => $this->dictionary,
            'lists' => $lists->get()
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {

        \PermissionsController::allowPermission('core_dictionaries', 'create');
        $this->dictionary->structure = json_decode($this->dictionary->structure, TRUE);
        return view('core_dictionaries_views::lists.create', ['dictionary' => $this->dictionary]);
    }

    /**
     * @param null $dictionary_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store($dictionary_id = NULL) {

        \PermissionsController::allowPermission('core_dictionaries', 'create');
        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getStoreRules())):
            $list = $this->model->insert($request);
            if($request::has('fields')):
                foreach($request::input('fields') as $field_name => $field_value):
                    if(is_array($field_value) && !empty($field_value)):
                        $field_value = implode(',', $field_value);
                    endif;
                    $this->fields->insert(['dictionary_list_id' => $list->id, 'field' => $field_name, 'value' => $field_value]);
                endforeach;
            endif;
            $this->uploadFiles($request, $list->id);
            return \ResponseController::success(201)->redirect(route('core.dictionaries.lists_index', $dictionary_id))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param null $dictionary_id
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($dictionary_id = NULL, $id = NULL) {

        \PermissionsController::allowPermission('core_dictionaries', 'edit');
        $item = $this->model->findOrFail($id);
        $fields = $this->fields->whereDictionaryListId($item->id)->lists('value', 'field');
        $structure = [];
        foreach(json_decode($this->dictionary->structure, TRUE) as $index => $element):
            $structure[$index] = $element;
            $structure[$index]['value'] = isset($fields[$element['name']]) ? $fields[$element['name']] : NULL;
        endforeach;
        $this->dictionary->structure = $structure;
        return view('core_dictionaries_views::lists.edit', ['dictionary' => $this->dictionary, 'item' => $item]);
    }

    /**
     * @param null $dictionary_id
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($dictionary_id = NULL, $id = NULL) {

        \PermissionsController::allowPermission('core_dictionaries', 'edit');
        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $this->model->replace($id, $request);
            if($request::has('fields')):
                foreach($request::input('fields') as $field_name => $field_value):
                    if(is_array($field_value) && !empty($field_value)):
                        $field_value = implode(',', $field_value);
                    endif;
                    $this->fields->replace(['dictionary_list_id' => $id, 'field' => $field_name], $field_value);
                endforeach;
            endif;
            $this->uploadFiles($request, $id);
            return \ResponseController::success(202)->redirect(route('core.dictionaries.lists_index', $dictionary_id))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param null $dictionary_id
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($dictionary_id = NULL, $id = NULL) {

        \PermissionsController::allowPermission('core_dictionaries', 'delete');
        \RequestController::isAJAX()->init();
        $this->deleteFiles($id);
        $this->model->remove($id);
        return \ResponseController::success(1203)->redirect(route('core.dictionaries.lists_index', $dictionary_id))->json();
    }

    /**************************************************************************************************************/
    /**
     * Загрузка файлов
     * @param \Request $request
     * @param $dictionary_list_id
     * @return mixed
     */
    private function uploadFiles(\Request $request, $dictionary_list_id) {

        if($this->dictionary->structure = json_decode($this->dictionary->structure, TRUE)):
            foreach($this->dictionary->structure as $structure):
                if(in_array($structure['type'], ['image', 'file'])):
                    if($request::hasFile($structure['name']) && $request::file($structure['name'])->isValid()):
                        $upload_directory = 'dictionaries';
                        $fileName = time()."_".rand(1000, 1999).'.'.$request::file($structure['name'])->getClientOriginalExtension();
                        $request::file($structure['name'])->move('uploads/'.$upload_directory, $fileName);
                        $image = add_first_slash($upload_directory.'/'.$fileName);
                        if($field = $this->fields->where('dictionary_list_id', $dictionary_list_id)->where('field', $structure['name'])->first()):
                            if(\Storage::exists($field->value)):
                                \Storage::delete($field->value);
                            endif;
                            $this->fields->replace(['dictionary_list_id' => $dictionary_list_id, 'field' => $structure['name']], $image);
                        else:
                            $this->fields->insert(['dictionary_list_id' => $dictionary_list_id, 'field' => $structure['name'], 'value' => $image]);
                        endif;
                    endif;
                endif;
            endforeach;
        endif;
    }

    /**
     * Удаление файлов
     * @param $dictionary_list_id
     */
    private function deleteFiles($dictionary_list_id) {

        if($this->dictionary->structure = json_decode($this->dictionary->structure, TRUE)):
            foreach($this->dictionary->structure as $structure):
                if(in_array($structure['type'], ['image', 'file'])):
                    if($field = $this->fields->where('dictionary_list_id', $dictionary_list_id)->where('field', $structure['name'])->first()):
                        if(\Storage::exists($field->value)):
                            \Storage::delete($field->value);
                        endif;
                    endif;
                endif;
            endforeach;
        endif;
    }

    /**
     * Сортировка элементов словаря
     * @return \Illuminate\Http\JsonResponse
     */
    public function sortable() {

        \PermissionsController::allowPermission('core_dictionaries', 'dictionaries');
        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, ['elements' => 'required'])):
            foreach(explode(',', $request::input('elements')) as $index => $slide_id):
                $this->model->where('id', $slide_id)->update(['order' => $index + 1]);
            endforeach;
            return \ResponseController::success(202)->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }
}