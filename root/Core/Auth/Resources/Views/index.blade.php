@extends('root_views::layouts.auth')
@section('title', trans('core_auth_lang::index.page_title'))
@section('description', trans('core_auth_lang::index.page_description'))
@section('content')
    <div class="login-content">
        <div class="lc-block toggled" id="l-login">
            <div class="lcb-form">
                <div class="list-group lg-odd-black">
                    <div class="clearfix p-0 m-h-30">
                        <div class="ah-label hidden-xs">@lang('core_auth_lang::index.form.title')</div>
                    </div>
                </div>
                {!! Form::open(['route' => 'auth.login.authenticate', 'class' => 'form-validate', 'id' => 'auth-form']) !!}
                {!! Form::hidden('redirect', session('auth.redirect')) !!}
                {!! Form::hidden('remember', TRUE) !!}
                <div class="p-t-25 p-r-10 p-l-30">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::email('login', NULL, ['class' => 'input-sm form-control fg-input']) !!}
                            <label class="fg-label">
                                <i class="zmdi zmdi-email"></i> @lang('core_auth_lang::index.form.login_field')
                            </label>
                        </div>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::password('password', ['class' => 'input-sm form-control fg-input']) !!}
                            <label class="fg-label">
                                <i class="zmdi zmdi-key"></i> @lang('core_auth_lang::index.form.password')
                            </label>
                        </div>
                    </div>
                </div>
                {!! Form::button('<i class="zmdi zmdi-arrow-forward"></i>', ['type' => 'submit', 'class' => 'btn btn-login btn-success btn-float waves-effect waves-circle waves-float', 'autocomplete' => 'off']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop