<?php
namespace STALKER_CMS\Core\Auth\Http\Controllers;

use STALKER_CMS\Vendor\Http\Controllers\Controller;

/**
 * Class ModuleController
 * @package STALKER_CMS\Core\Auth\Http\Controllers
 */
abstract class ModuleController extends Controller {

}