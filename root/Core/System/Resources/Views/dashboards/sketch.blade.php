<?php
$history = [];
if(\PermissionsController::isPackageEnabled('core_content')):
    $page_access = FALSE;
    if(\PermissionsController::allowPermission('core_content', 'edit', FALSE)):
        $page_access = TRUE;
    endif;
    foreach(\STALKER_CMS\Core\Content\Models\Page::orderBy('updated_at', 'desc')->whereLocale(\App::getLocale())->take(3)->with('author')->get() as $page):
        if(!empty($page->author)):
            $updated_at = $page->updated_at->timestamp;
            $history[$page->updated_at->timestamp] = [
                    'package' => array_translate(config('core_content::menu.menu_child.pages.title')),
                    'slug' => $page->slug,
                    'title' => $page->title,
                    'updated_at' => $page->updated_at->format('d.m.Y H:i'),
                    'edit_access' => $page_access,
                    'link_source' => route('core.content.pages.edit', $page->id),
                    'link_title' => trans('core_content_lang::pages.edit'),
                    'author' => $page->author->name
            ];
        endif;
    endforeach;
    foreach(\STALKER_CMS\Core\Content\Models\PageBlock::orderBy('updated_at', 'desc')->with('author')->take(3)->get() as $block):
        if(!empty($block->author)):
            $history[$block->updated_at->timestamp] = [
                    'package' => trans('core_content_lang::pages.blocks'),
                    'slug' => $block->slug,
                    'title' => $block->title,
                    'updated_at' => $block->updated_at->format('d.m.Y H:i'),
                    'edit_access' => $page_access,
                    'link_source' => route('core.content.pages.blocks_edit', [$block->page_id, $block->id]),
                    'link_title' => trans('core_content_lang::blocks.edit'),
                    'author' => $block->author->name
            ];
        endif;
    endforeach;
endif;
krsort($history);
?>
<div class="list-group lg-odd-black">
    <div class="action-header clearfix">
        <div class="ah-label hidden-xs">@lang('core_system_lang::dashboard.history.title')</div>
    </div>
    <div class="card-body card-padding m-h-250 p-0">
        @forelse($history as $record)
            <div class="list-group-item media">
                @if($record['edit_access'])
                    <div class="pull-right">
                        <div class="actions dropdown">
                            <a aria-expanded="true" data-toggle="dropdown" href="">
                                <i class="zmdi zmdi-more-vert"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="{!! $record['link_source'] !!}">
                                        {!! $record['link_title'] !!}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                @endif
                <div class="media-body">
                    <div class="lgi-heading">{!! $record['title'] !!}</div>
                    <small class="lgi-text">{!! $record['package'] !!}</small>
                    <ul class="lgi-attrs">
                        <li>@lang('core_system_lang::dashboard.history.updated'): {!! $record['updated_at'] !!}</li>
                        <li>@lang('core_system_lang::dashboard.history.author'): {!! $record['author'] !!}</li>
                    </ul>
                </div>
            </div>
        @empty
            <h2 class="f-16 c-gray m-l-30">@lang('core_system_lang::dashboard.history.empty')</h2>
        @endforelse
    </div>
</div>