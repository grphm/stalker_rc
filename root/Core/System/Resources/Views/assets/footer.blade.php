<footer id="footer">
    Copyright &copy; 2016{!! Carbon\Carbon::now()->year > 2016 ? ' - ' . Carbon\Carbon::now()->year : '' !!} {!! config('app.application_name') !!}
</footer>