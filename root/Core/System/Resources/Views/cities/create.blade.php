@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_system::menu.icon') }}"></i> {!! array_translate(config('core_system::menu.title')) !!}
        </li>
        <li>
            <a href="{!! route('core.system.cities.index') !!}">
                <i class="{{ config('core_system::menu.menu_child.cities.icon') }}"></i> {!! array_translate(config('core_system::menu.menu_child.cities.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-plus"></i> @lang('core_system_lang::cities.insert.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2><i class="zmdi zmdi-plus"></i> @lang('core_system_lang::cities.insert.title')</h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            <div class="row">
                {!! Form::open(['route' => 'core.system.cities.store', 'class' => 'form-validate', 'id' => 'add-city-form', 'method' => 'POST']) !!}
                <div class="col-sm-6">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                            <label class="fg-label">@lang('core_system_lang::cities.insert.form.title')</label>
                        </div>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('slug', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                            <label class="fg-label">@lang('core_system_lang::cities.insert.form.slug')</label>
                        </div>
                        <small class="help-description">
                            @lang('core_system_lang::cities.insert.form.slug_help_description')
                        </small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                        <i class="fa fa-save"></i>
                        <span class="btn-text">@lang('core_system_lang::cities.insert.form.submit')</span>
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop