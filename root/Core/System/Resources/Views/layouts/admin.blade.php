<!DOCTYPE html>
<!--[if IE 9 ]>
<html class="ie9"><![endif]-->
<head>
    {!! Html::meta(['charset' => 'utf-8', 'content' => NULL, 'name' => NULL]) !!}
    {!! Html::meta(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1']) !!}
    <title>@yield('title', config('app.application_name'))</title>
    <meta name="description" content="@yield('description')">
    {!! Html::meta(['name' => 'csrf-token', 'content' => csrf_token()]) !!}
    {!! Html::meta(['name' => 'locale', 'content' => \App::getLocale()]) !!}
    {!! Html::meta(['name' => 'fallback_locale', 'content' => config('app.fallback_locale')]) !!}
    {!! Html::coreVendorCSS() !!}
    {!! Html::coreMainCSS() !!}
    {!! Html::favicon() !!}
    @yield('styles')
    @if(Session::has('status'))
        {!! Html::meta(['name' => 'flash-message', 'content' => trans('root_lang::codes.'.Session::get('status'))]) !!}
    @endif
    @if(\Request::has('status'))
        {!! Html::meta(['name' => 'flash-message', 'content' => trans('root_lang::codes.'.\Request::input('status'))]) !!}
    @endif
</head>
<body class="scroll-overflow">
@include('core_system_views::assets.header')
<section id="main">
    @include('core_system_views::assets.aside')
    <section id="content">
        <div class="container">
            @yield('breadcrumb')
            @yield('content')
            @yield('modal')
        </div>
    </section>
</section>
@include('core_system_views::assets.footer')
@include('root_views::assets.old-browser')
@preloader()
{!! Html::coreVendorJS() !!}
@yield('scripts_before')
{!! Html::coreMainJS() !!}
@yield('scripts_after')
<!-- Placeholder for IE9 -->
<!--[if IE 9 ]>
{!! Html::script('core/scripts/jquery.placeholder.min.js') !!}
<![endif]-->
</body>
</html>