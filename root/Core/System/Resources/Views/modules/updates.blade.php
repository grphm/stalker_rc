@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_system::menu.icon') }}"></i> {!! array_translate(config('core_system::menu.title')) !!}
        </li>
        <li>
            <a href="{!! route('core.system.modules.index') !!}">
                <i class="{{ config('core_system::menu.menu_child.modules.icon') }}"></i> {!! array_translate(config('core_system::menu.menu_child.modules.title')) !!}
            </a>
        </li>
        <li class="active">
            @lang('core_system_lang::modules.updates.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>@lang('core_system_lang::modules.updates.breadcrumb')</h2>
    </div>
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">
                    @lang('core_system_lang::modules.updates.core_list')
                </div>
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                {!! Form::open(['route' => 'core.system.modules.core_updates', 'role' => 'form', 'id' => 'core-update-form']) !!}
                @foreach($packages as $package)
                    <div class="list-group-item media">
                        <div class="checkbox pull-left">
                            <label>
                                {!! Form::checkbox('packages[]', $package->id, FALSE, ['autocomplete' => 'off']) !!}
                                <i class="input-helper"></i>
                            </label>
                        </div>
                        <div class="media-body">
                            <div class="lgi-heading">
                                <i class="{!! config($package->slug . '::config.package_icon' ) !!}"></i>
                                {!! $package->title !!}
                            </div>
                            <small class="lgi-text">{!! $package->description !!}</small>
                            <ul class="lgi-attrs">
                                @if($package->enabled)
                                    <li>
                                        @lang('core_system_lang::modules.updates.package_version'):
                                        {!! number_format((float) config($package->slug . '::config.version.ver' ), 1) !!}
                                    </li>
                                    <li>
                                        @lang('core_system_lang::modules.updates.package_path'):
                                        root/{!! $package->install_path !!}
                                    </li>
                                @else
                                    <li class="bg-info">
                                        @lang('core_system_lang::modules.updates.package_disabled')
                                    </li>
                                @endif
                                @if(\Request::has('core'))
                                    <?php $packages_ids = explode(',', \Request::input('core')); ?>
                                    @if(!empty($packages_ids) && in_array($package->id, $packages_ids))
                                        <li class="bg-success">
                                            @lang('core_system_lang::modules.updates.package_updated')
                                        </li>
                                    @endif
                                @endif
                            </ul>
                        </div>
                    </div>
                @endforeach
                <div class="m-l-30 m-t-10 m-b-20">
                    <button class="btn btn-primary btn-sm waves-effect js-wait-action" autocomplete="off" type="submit">
                        @lang('core_system_lang::modules.updates.package_core_submit')
                    </button>
                </div>
                {!! Form::close() !!}
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">
                    @lang('core_system_lang::modules.updates.solutions_list')
                </div>
            </div>
            <div class="card-body card-padding m-h-100 p-0">
                @if($solutions->count())
                    {!! Form::open(['route' => 'core.system.modules.solutions_updates', 'role' => 'form', 'id' => 'solutions-update-form']) !!}
                    @foreach($solutions as $package)
                        <div class="list-group-item media">
                            <div class="checkbox pull-left">
                                <label>
                                    {!! Form::checkbox('packages[]', $package->id, FALSE, ['autocomplete' => 'off']) !!}
                                    <i class="input-helper"></i>
                                </label>
                            </div>
                            <div class="media-body">
                                <div class="lgi-heading">
                                    <i class="{!! config($package->slug . '::config.package_icon' ) !!}"></i>
                                    {!! $package->title !!}
                                </div>
                                <small class="lgi-text">{!! $package->description !!}</small>
                                <ul class="lgi-attrs">
                                    @if($package->enabled)
                                        <li>
                                            @lang('core_system_lang::modules.updates.package_version'):
                                            {!! number_format((float) config($package->slug . '::config.version.ver' ), 1) !!}
                                        </li>
                                        <li>
                                            @lang('core_system_lang::modules.updates.package_path'):
                                            root/{!! $package->install_path !!}
                                        </li>
                                    @else
                                        <li class="bg-info">
                                            @lang('core_system_lang::modules.updates.package_disabled')
                                        </li>
                                    @endif
                                    @if(\Request::has('solutions'))
                                        <?php $packages_ids = explode(',', \Request::input('solutions')); ?>
                                        @if(!empty($packages_ids) && in_array($package->id, $packages_ids))
                                            <li class="bg-success">
                                                @lang('core_system_lang::modules.updates.package_updated')
                                            </li>
                                        @endif
                                    @endif
                                </ul>
                            </div>
                        </div>
                    @endforeach
                    <div class="m-l-30 m-t-10 m-b-20">
                        <button class="btn btn-primary btn-sm waves-effect js-wait-action" autocomplete="off" type="submit">
                            @lang('core_system_lang::modules.updates.package_solution_submit')
                        </button>
                    </div>
                    {!! Form::close() !!}
                    <div class="clearfix"></div>
                @else
                    <h2 class="f-16 c-gray m-l-30">@lang('core_system_lang::modules.updates.solutions_list_empty')</h2>
                @endif
            </div>
        </div>
    </div>
@stop
@section('scripts_after')
    <script>
        $(function () {
            $(".js-wait-action").click(function () {
                var translate = {
                    "ru": "&nbsp;Пожалуйста подождите. Скачиваются обновления&nbsp;",
                    "en": "&nbsp;Please wait. Download updates&nbsp;",
                    "es": "&nbsp;Por favor espere. Descargar actualizaciones&nbsp;"
                }
                var content = '<i class="fa fa-spinner fa-pulse p-3"></i>' + translate[BASIC.locale];
                $(this).after('<a href="javascript:void(0);">' + content + '</a>');
                $(this).addClass('hidden');
            })
        });
    </script>
@stop