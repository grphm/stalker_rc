<?php
return [
    'select_all' => 'Выбрать все',
    'boot' => 'Последовательность загрузки',
    'information' => 'Информация',
    'update' => 'Обновления CMS',
    'required' => 'Для установки требуются',
    'clear_cache_notification_part1' => 'Перед установкой модулей очистите кэш',
    'clear_cache_notification_part2' => 'настроек',
    'updates' => [
        'breadcrumb' => 'Обновление CMS',
        'core_list' => 'Модули ядра',
        'solutions_list' => 'Готовые решения',
        'solutions_list_empty' => 'Готовые решения не установлены',
        'package_version' => 'Версия пакета',
        'package_path' => 'Каталог пакета',
        'package_disabled' => 'Пакет выключен',
        'package_updated' => 'Пакет обновлен',
        'package_core_submit' => 'Обновить модули ядра',
        'package_solution_submit' => 'Обновить готовые решения',
    ],
    'disabled' => [
        'question' => 'Отключить модуль',
        'confirmbuttontext' => 'Да, отключить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Отключить',
    ]
];