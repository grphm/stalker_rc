<?php
return [
    'list' => 'Список доступных языков',
    'edit' => 'Редактировать',
    'empty' => 'Список пустой',
    'slug' => 'Символьный код',
    'default' => 'Основной язык',
    'active' => 'Активен',
    'delete' => [
        'question' => 'Удалить язык',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить',
    ],
    'insert' => [
        'breadcrumb' => 'Добавить',
        'title' => 'Добавление языка',
        'form' => [
            'title' => 'Название',
            'slug' => 'Символьный код',
            'slug_help_description' => 'Только латинские символы, символы подчеркивания, тире, не менее 5 символов. <br>Например: ru',
            'code' => 'Код',
            'active' => 'Активный',
            'submit' => 'Сохранить'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Редактировать',
        'title' => 'Редактирование языка',
        'form' => [
            'title' => 'Название',
            'slug' => 'Символьный код',
            'slug_help_description' => 'Только латинские символы, символы подчеркивания, тире, не менее 5 символов. <br>Например: ru',
            'code' => 'Код',
            'active' => 'Активный',
            'submit' => 'Сохранить'
        ]
    ]
];