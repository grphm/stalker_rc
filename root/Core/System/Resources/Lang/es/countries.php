<?php
return [
    'list' => 'Lista de países',
    'search' => 'Introduzca el nombre del país',
    'edit' => 'Editar',
    'empty' => 'Lista está vacía',
    'import' => 'Lista de importación',
    'delete' => [
        'question' => 'Eliminar los país',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar'
    ],
    'insert' => [
        'breadcrumb' => 'Añadir',
        'title' => 'Agregar una país',
        'form' => [
            'title' => 'Título',
            'slug' => 'Simbólico código',
            'slug_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres. <br> Ejemplo: moscow',
            'submit' => 'Guardar'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Editar',
        'title' => 'Edición de país',
        'form' => [
            'title' => 'Título',
            'slug' => 'Simbólico código',
            'slug_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres. <br> Ejemplo: moscow',
            'submit' => 'Guardar'
        ]
    ]
];