<?php
return [
    'select_all' => 'Seleccionar todo',
    'boot' => 'Secuencia de inicio',
    'information' => 'Información',
    'update' => 'Actualizaciones CMS',
    'required' => 'Necesario para la instalación',
    'clear_cache_notification_part1' => 'Antes de instalar los módulos, borrar la memoria caché',
    'clear_cache_notification_part2' => 'ajustes',
    'updates' => [
        'breadcrumb' => 'Actualizaciones CMS',
        'core_list' => 'Módulos del núcleo',
        'solutions_list' => 'Soluciones',
        'solutions_list_empty' => 'Soluciones listas no están instalados',
        'package_version' => 'Versión del paquete',
        'package_path' => 'Catálogo de patskage',
        'package_disabled' => 'El paquete está deshabilitado',
        'package_updated' => 'Paquete actualizado',
        'package_core_submit' => 'Actualizar paquetes principales',
        'package_solution_submit' => 'Soluciones de actualización'
    ],
    'disabled' => [
        'question' => 'Módulo desactivar',
        'confirmbuttontext' => 'Sí, deshabilitar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Inhabilitar',
    ]
];