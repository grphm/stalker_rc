<?php
return [
    'list' => 'Lista de ciudades',
    'search' => 'Introduce el nombre de la ciudad',
    'edit' => 'Editar',
    'empty' => 'Lista está vacía',
    'import' => 'Lista de importación',
    'delete' => [
        'question' => 'Eliminar los la ciudad',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar'
    ],
    'insert' => [
        'breadcrumb' => 'Añadir',
        'title' => 'Agregar una ciudad',
        'form' => [
            'title' => 'Título',
            'slug' => 'Simbólico código',
            'slug_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres. <br> Ejemplo: moscow',
            'submit' => 'Guardar'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Editar',
        'title' => 'Edición de la ciudad',
        'form' => [
            'title' => 'Título',
            'slug' => 'Simbólico código',
            'slug_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres. <br> Ejemplo: moscow',
            'submit' => 'Guardar'
        ]
    ]
];