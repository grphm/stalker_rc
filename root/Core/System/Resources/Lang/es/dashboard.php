<?php
return [
    'control_panel' => 'Panel de control',
    'edit_profile' => 'Editar perfil',
    'edit_profile_permissions' => 'Configuración de acceso',
    'history' => [
        'title' => 'Hanges Lust',
        'empty' => 'Lista está vacía',
        'updated' => 'Fecha de actualización',
        'author' => 'Author'
    ],
    'header' => [
        'notification' => 'Notificaciones',
        'notification_view_all' => 'Leer todo',
        'settings' => [
            'full_screen' => 'Pantalla completa',
            'edit' => 'Ajustes',
            'logout' => 'Cerrar sesión',
        ],
        'debug_mode' => 'Modo de depuración',
        'services_mode' => 'Modo de mantenimiento',
        'config_cached' => 'Configuración de los módulos en caché',
        'config_no_cached' => 'Configuración de los módulos no almacenada en caché'
    ],
    'title' => 'Cuadros de mando',
    'feedback' => [
        'title' => 'Retroalimentación',
        'inbox' => 'Recibido',
        'views' => 'Vistas',
        'sends' => 'Las respuestas',
    ],
    'feedback_list' => [
        'title' => 'Retroalimentación',
        'show_all' => 'Lee todo'
    ],
    'edit_template' => 'Editar plantilla'
];