<?php
return [
    'select_all' => 'Select all',
    'boot' => 'Boot Sequence',
    'information' => 'Information',
    'update' => 'CMS Updates',
    'required' => 'Required for installation',
    'clear_cache_notification_part1' => 'Before installing the modules, clear the cache',
    'clear_cache_notification_part2' => 'settings',
    'updates' => [
        'breadcrumb' => 'CMS Updates',
        'core_list' => 'Core modules',
        'solutions_list' => 'Solutions',
        'solutions_list_empty' => 'Solutions are not installed',
        'package_version' => 'Package version',
        'package_path' => 'Catalog package',
        'package_disabled' => 'Package is disabled',
        'package_updated' => 'Package updated',
        'package_core_submit' => 'Update core packages',
        'package_solution_submit' => 'Update solutions',
    ],
    'disabled' => [
        'question' => 'Disable module',
        'confirmbuttontext' => 'Yes, disable',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Disable',
    ]
];