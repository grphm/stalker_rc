<?php
return [
    'control_panel' => 'Control Panel',
    'edit_profile' => 'Edit profile',
    'edit_profile_permissions' => 'Access Settings',
    'history' => [
        'title' => 'Last changes',
        'empty' => 'List is empty',
        'updated' => 'Update date',
        'author' => 'Author'
    ],
    'header' => [
        'notification' => 'Notifications',
        'notification_view_all' => 'Read all',
        'settings' => [
            'full_screen' => 'Full Screen',
            'edit' => 'Settings',
            'logout' => 'Log out',
        ],
        'debug_mode' => 'Debug mode',
        'services_mode' => 'Maintenance mode',
        'config_cached' => 'Modules settings cached',
        'config_no_cached' => 'Modules settings not cached'
    ],
    'title' => 'Dashboard',
    'feedback' => [
        'title' => 'Feedback',
        'inbox' => 'Received',
        'views' => 'Views',
        'sends' => 'Answer',
    ],
    'feedback_list' => [
        'title' => 'Feedback',
        'show_all' => 'Read all'
    ],
    'edit_template' => 'Edit template'
];