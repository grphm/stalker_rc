<?php
namespace STALKER_CMS\Core\System\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Фасад контроллера доступов
 * Class PermissionsController
 * @package STALKER_CMS\Core\System\Facades
 */
class PermissionsController extends Facade {

    protected static function getFacadeAccessor() {

        return 'PermissionsController';
    }
}