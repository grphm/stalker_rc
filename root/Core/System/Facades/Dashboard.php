<?php
namespace STALKER_CMS\Core\System\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Фасад контроллера Dashboard
 * Class Dashboard
 * @package STALKER_CMS\Core\Auth\Facades
 */
class Dashboard extends Facade {

    protected static function getFacadeAccessor() {

        return 'DashboardController';
    }
}