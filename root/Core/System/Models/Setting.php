<?php
namespace STALKER_CMS\Core\System\Models;

use STALKER_CMS\Vendor\Models\BaseModel;

/**
 * Модель Настройки
 * Class Setting
 * @package STALKER_CMS\Core\System\Models
 */
class Setting extends BaseModel {

    protected $table = 'system_settings';
    protected $fillable = ['package', 'module', 'name', 'value', 'user_id'];
    protected $guarded = [];
    public static $rules = [
        'package' => 'required', 'module' => 'required', 'name' => 'required', 'user_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }
}