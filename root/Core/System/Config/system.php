<?php
return [
    'package_name' => 'core_system',
    'package_title' => ['ru' => 'Системный модуль', 'en' => 'System module', 'es' => 'Módulo de sistema'],
    'package_icon' => 'zmdi zmdi-settings-square',
    'relations' => [],
    'package_description' => [
        'ru' => 'Основной модуль CMS',
        'en' => 'CMS basic module',
        'es' => 'Unidad básica CMS'
    ],
    'version' => [
        'ver' => 1.3,
        'date' => '12.01.2017'
    ]
];
