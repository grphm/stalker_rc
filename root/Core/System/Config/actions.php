<?php
return [
    'settings' => [
        'title' => [
            'ru' => 'Доступ к настройкам модулей',
            'en' => 'Access to the module configuration',
            'es' => 'El acceso a la configuración del módulo'
        ],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-settings'
    ],
    'modules' => [
        'title' => [
            'ru' => 'Работа с модулями',
            'en' => 'Manage Modules',
            'es' => 'Gestionar módulos'
        ],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-view-module'
    ],
    'modules_solutions' => [
        'title' => [
            'ru' => 'Работа с решениями',
            'en' => 'Working with solutions',
            'es' => 'Trabajar con soluciones'
        ],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-widgets'
    ],
    'groups' => [
        'title' => [
            'ru' => 'Работа с группами пользователей',
            'en' => 'Work with user groups',
            'es' => 'Trabaja con grupos de usuarios'
        ],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-accounts-list'
    ],
    'users' => [
        'title' => [
            'ru' => 'Работа с пользователями',
            'en' => 'Working with Users',
            'es' => 'Trabajando con Usuarios'
        ],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-accounts-alt'
    ],
    'countries' => [
        'title' => [
            'ru' => 'Список стран',
            'en' => 'List of countries',
            'es' => 'Lista de países'
        ],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-globe'
    ],
    'cities' => [
        'title' => [
            'ru' => 'Список городов',
            'en' => 'Lista de las ciudades',
            'es' => 'List of cities'
        ],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-city'
    ],
    'languages' => [
        'title' => [
            'ru' => 'Мультиязычность',
            'en' => 'Мultilingual',
            'es' => 'Мultilingües'
        ],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-translate'
    ]
];