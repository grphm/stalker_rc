<?php
namespace STALKER_CMS\Core\System\Seeds;

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ModuleTableSeeder extends Seeder {

    public function run() {

        \DB::table('system_groups')->insert([
            'slug' => 'developer', 'title' => json_encode(['ru' => 'Разработчики', 'en' => 'Developers', 'es' => 'Desarrolladores']),
            'dashboard' => 'admin', 'start_url' => 'admin', 'required' => 1,
            'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        \DB::table('system_groups')->insert([
            'slug' => 'admin', 'title' => json_encode(['ru' => 'Администраторы', 'en' => 'Administrators', 'es' => 'Administradores']),
            'dashboard' => 'admin', 'start_url' => 'admin', 'required' => 0,
            'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        \DB::table('system_groups')->insert([
            'slug' => 'moderator', 'title' => json_encode(['ru' => 'Модераторы', 'en' => 'Moderators', 'es' => 'Moderadores']),
            'dashboard' => 'admin', 'start_url' => 'admin', 'required' => 0,
            'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        \DB::table('users')->insert([
            'group_id' => 1, 'locale' => env('APP_LOCALE', 'ru'), 'name' => 'Разработчик', 'login' => 'developer@test.ru', 'email' => 'developer@test.ru', 'active' => 1,
            'password' => bcrypt('grapheme1234'), 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        \DB::table('users')->insert([
            'group_id' => 2, 'locale' => env('APP_LOCALE', 'ru'), 'name' => 'Администратор', 'login' => 'admin@test.ru', 'email' => 'admin@test.ru', 'active' => 1,
            'password' => bcrypt('000000'), 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        \DB::table('users')->insert([
            'group_id' => 3, 'locale' => env('APP_LOCALE', 'ru'), 'name' => 'Модератор', 'login' => 'moder@test.ru', 'email' => 'moder@test.ru', 'active' => 1,
            'password' => bcrypt('111111'), 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        $actions = [
            ['group_id' => 1, 'module' => 'core_system', 'action' => 'settings', 'status' => TRUE],
            ['group_id' => 1, 'module' => 'core_system', 'action' => 'modules', 'status' => TRUE],
            ['group_id' => 1, 'module' => 'core_system', 'action' => 'modules_solutions', 'status' => TRUE],
            ['group_id' => 1, 'module' => 'core_system', 'action' => 'groups', 'status' => TRUE],
            ['group_id' => 1, 'module' => 'core_system', 'action' => 'users', 'status' => TRUE],
            ['group_id' => 1, 'module' => 'core_system', 'action' => 'countries', 'status' => TRUE],
            ['group_id' => 1, 'module' => 'core_system', 'action' => 'cities', 'status' => TRUE],
            ['group_id' => 1, 'module' => 'core_system', 'action' => 'languages', 'status' => TRUE],
        ];
        foreach($actions as $action):
            \DB::table('system_permissions')->insert($action);
        endforeach;
        $settings = [
            ['package' => 'core_system', 'module' => 'settings', 'name' => 'debug_mode', 'value' => env('APP_DEBUG', FALSE), 'user_id' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['package' => 'core_system', 'module' => 'settings', 'name' => 'services_mode', 'value' => FALSE, 'user_id' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ];
        foreach($settings as $setting):
            \DB::table('system_settings')->insert($setting);
        endforeach;
        \DB::table('system_countries')->insert(['locale' => 'ru', 'slug' => 'Не указано', 'title' => 'Не указано']);
        \DB::table('system_countries')->insert(['locale' => 'en', 'slug' => 'Not specified', 'title' => 'Not specified']);
        \DB::table('system_countries')->insert(['locale' => 'es', 'slug' => 'No especificada', 'title' => 'No especificada']);
        \DB::table('system_cities')->insert(['locale' => 'ru', 'slug' => 'Не указано', 'title' => 'Не указано']);
        \DB::table('system_cities')->insert(['locale' => 'en', 'slug' => 'Not specified', 'title' => 'Not specified']);
        \DB::table('system_cities')->insert(['locale' => 'es', 'slug' => 'No especificada', 'title' => 'No especificada']);
    }
}