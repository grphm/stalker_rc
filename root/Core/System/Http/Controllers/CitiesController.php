<?php
namespace STALKER_CMS\Core\System\Http\Controllers;

use STALKER_CMS\Core\System\Models\Cities;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

/**
 * Контроллер городов
 * Class UsersController
 * @package STALKER_CMS\Core\System\Http\Controllers
 */
class CitiesController extends ModuleController implements CrudInterface {

    protected $model;

    /**
     * CitiesController constructor.
     * @param Cities $city
     */
    public function __construct(Cities $city) {

        $this->model = $city;
        \PermissionsController::allowPermission('core_system', 'cities');
        $this->middleware('auth');
    }

    /**
     * Список городов
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        $request = \RequestController::init();
        $cities = $this->model->whereLocale(\App::getLocale());
        if($request::has('search')):
            $search = $request::get('search');
            $cities = $cities->where(function($query) use ($search) {

                $query->orWhere('title', 'like', '%'.$search.'%');
            });
        endif;
        $cities = $cities->orderBy('title')->paginate(25);
        return view('core_system_views::cities.index', compact('cities'));
    }

    /**
     * Добавление города
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {

        return view('core_system_views::cities.create');
    }

    /**
     * Сохранение города
     * @return mixed
     */
    public function store() {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getStoreRules())):
            $this->model->insert($request);
            return \ResponseController::success(201)->redirect(route('core.system.cities.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Редактирование города
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id) {

        $city = $this->model->findOrFail($id);
        return view('core_system_views::cities.edit', compact('city'));
    }

    /**
     * Обновление города
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update($id) {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $this->model->replace($id, $request);
            return \ResponseController::success(200)->redirect(route('core.system.cities.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Удаление города
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {

        \RequestController::isAJAX()->init();
        $this->model->remove($id);
        return \ResponseController::success(1203)->redirect(route('core.system.cities.index'))->json();
    }

    /*****************************************************************************/
    public function import() {

        $request = \RequestController::init();
        if($request::hasFile('import') && $request::file('import')->isValid()):
            if($csv_records = csv_to_array($request::file('import')->getRealPath())):
                foreach($csv_records as $index => $record):
                    foreach($record as $value):
                        $slug = transliteration($value);
                        if($this->model->whereLocale(\App::getLocale())->whereSlug($slug)->exists() === FALSE):
                            Cities::create(['slug' => $slug, 'locale' => \App::getLocale(), 'title' => $value]);
                        endif;
                    endforeach;
                endforeach;
            endif;
        endif;
        return redirect()->route('core.system.cities.index');
    }
}