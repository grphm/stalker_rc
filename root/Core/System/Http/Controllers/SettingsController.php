<?php
namespace STALKER_CMS\Core\System\Http\Controllers;

use STALKER_CMS\Core\System\Models\Setting;
use STALKER_CMS\Core\System\Models\Languages;

/**
 * Контроллер Настройки пакетов
 * Class SettingsController
 * @package STALKER_CMS\Core\System\Http\Controllers
 */
class SettingsController extends ModuleController {

    /**
     * SettingsController constructor.
     */
    public function __construct() {

        $this->middleware('auth');
    }

    /**
     * Страница настроек установленных модулей
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        \PermissionsController::allowPermission('core_system', 'settings');
        $settings_values = \App::make('Settings');
        return view('core_system_views::settings.index', compact('settings_values'));
    }

    /**
     * Сохранение настроек
     * Сбрасываем все чекбоксы
     * Устанавливаем текущие
     * Анализируем состояние
     * @return \Illuminate\Http\JsonResponse
     */
    public function update() {

        \PermissionsController::allowPermission('core_system', 'settings');
        $request = \RequestController::isAJAX()->init();
        $this->resetCheckboxSettings();
        foreach(\PermissionsController::packagesEnabled() as $package_name => $package_title):
            if($request::has($package_name)):
                foreach($request::input($package_name) as $module_name => $settings):
                    foreach($settings as $name => $value):
                        Setting::updateOrCreate(
                            ['package' => $package_name, 'module' => $module_name, 'name' => $name],
                            ['value' => $value, 'user_id' => \Auth::user()->id]
                        );
                    endforeach;
                endforeach;
            endif;
        endforeach;
        $this->analysisSystemSettings();
        $this->analysisMailsSettings();
        return \ResponseController::success(202)->set('responseText', FALSE)->redirect(route('core.system.settings.rebuild'))->json();
    }

    /**
     * Кэширование настроек
     * @return \Illuminate\Http\RedirectResponse
     */
    public function rebuild() {

        \PermissionsController::allowPermission('core_system', 'settings');
        \Artisan::call('config:cache', ['--no-ansi' => TRUE, '--quiet' => TRUE]);
        return redirect()->to(route('core.system.settings.index').'?status=200');
    }

    /**
     * Основные настройки CMS
     */
    public function settings() {

        $languages = Languages::whereActive(TRUE)->lists('title', 'slug');
        return view('core_system_views::settings.main', compact('languages'));
    }

    /**
     * Сохранения настроек CMS
     */
    public function settingsStore() {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, ['APP_LOCALE' => 'required', 'APPLICATION_NAME' => 'required'])):
            \Auth::user()->locale = $request::input('APP_LOCALE');
            \Auth::user()->save();
            update_config_file('APPLICATION_NAME="'.config('app.application_name').'"', 'APPLICATION_NAME="'.$request::input('APPLICATION_NAME').'"');
            update_config_file('CORE_THEME="'.config('app.core_theme').'"', 'CORE_THEME="'.$request::input('CORE_THEME').'"');
            if($request::hasFile('logo') && $request::file('logo')->isValid()):
                if($request::file('logo')->getMimeType() == 'image/png'):
                    $fileName = public_path('theme/images/sidebar-logo.png');
                    if(file_exists($fileName)):
                        unlink($fileName);
                    endif;
                    $request::file('logo')->move(public_path('theme/images'), 'sidebar-logo.png');
                endif;
            endif;
            if($request::hasFile('favicon') && $request::file('favicon')->isValid()):
                if($request::file('favicon')->getMimeType() == 'image/png'):
                    $request::file('favicon')->move(storage_path('app'), 'source.png');
                    $this->makeIcoFavicon(storage_path('app/source.png'));
                    $this->makePngFavicon(storage_path('app/source.png'));
                    \File::delete(storage_path('app/source.png'));
                endif;
            endif;
            \Artisan::call('config:cache', ['--no-ansi' => TRUE, '--quiet' => TRUE]);
            return \ResponseController::success(202)->redirect(route('cms.settings.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Получить все настройки включенных модулей
     * @return array
     */
    public static function getSettings() {

        $_settings = [];
        foreach(\PermissionsController::packagesEnabled() as $package_name => $package_title):
            if(config($package_name.'::settings')):
                $_settings[$package_name] = config($package_name.'::settings');
            endif;
        endforeach;
        foreach(Setting::all() as $db_setting):
            if(isset($_settings[$db_setting->package][$db_setting->module]['options'][$db_setting->name]['value'])):
                $_settings[$db_setting->package][$db_setting->module]['options'][$db_setting->name]['value'] = $db_setting->value;
                $_settings[$db_setting->package][$db_setting->module]['options'][$db_setting->name]['user'] = $db_setting->user_id ? \DB::table('users')->value('name') : 'Значение по умолчанию';
                $_settings[$db_setting->package][$db_setting->module]['options'][$db_setting->name]['updated_at'] = $db_setting->user_id ? $db_setting->updated_at->format("d.m.Y в H:i") : 'Значение по умолчанию';
            endif;
        endforeach;
        return $_settings;
    }

    /**
     * Создает файл конфигрурации в кэше
     * bootstrap/cache/config.php
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createConfigCash() {

        return $this->rebuild();
    }

    /**
     * Удаляет файл конфигрурации из кэша
     * bootstrap/cache/config.php
     * @return \Illuminate\Http\RedirectResponse
     */
    public function clearConfigCash() {

        \PermissionsController::allowPermission('core_system', 'settings');
        \Artisan::call('config:clear', ['--no-ansi' => TRUE, '--quiet' => TRUE]);
        return redirect()->route('core.system.settings.index')->with('status', 200);
    }

    /**
     * Показывает странцу информации о PHP
     * @return bool
     */
    public function phpInformation() {

        phpinfo();
    }

    /**
     *  Сбрасываем все значения чекбоксов
     */
    private function resetCheckboxSettings() {

        $settings_values = \App::make('Settings');
        foreach($settings_values as $package_name => $modules):
            foreach($modules as $module_name => $settings):
                foreach($settings['options'] as $name => $value):
                    if(isset($value['type']) && $value['type'] == 'checkbox'):
                        Setting::wherePackage($package_name)->whereModule($module_name)->whereName($name)->update(['value' => 0]);
                    endif;
                endforeach;
            endforeach;
        endforeach;
    }

    /**
     * Анализ значений чекбоксов
     */
    private function analysisSystemSettings() {

        if(\PermissionsController::isPackageEnabled('core_system')):
            $settings_values = self::getSettings();
            $settings_options = $settings_values['core_system']['settings']['options'];
            if($settings_options['debug_mode']['value'] == TRUE):
                update_config_file('APP_DEBUG=false', 'APP_DEBUG=true');
            elseif($settings_options['debug_mode']['value'] == FALSE):
                update_config_file('APP_DEBUG=true', 'APP_DEBUG=false');
            endif;
            \Artisan::call('config:clear', ['--no-ansi' => TRUE, '--quiet' => TRUE]);
        endif;
    }

    /**
     * Анализ и изменения настроек SMTP сервера
     */
    private function analysisMailsSettings() {

        if(\PermissionsController::isPackageEnabled('core_mailer')):
            $settings_values = self::getSettings();
            $settings_options = $settings_values['core_mailer']['mailer']['options'];
            $config_file = file(base_path('.env'));
            if(is_array($config_file)):
                foreach($config_file as $key => $value):
                    $params = explode('=', $value);
                    if(in_array('MAIL_HOST', $params)):
                        $config_file[$key] = 'MAIL_HOST='.$settings_options['smtp_host']['value']."\n";
                    elseif(in_array('MAIL_PORT', $params)):
                        $config_file[$key] = 'MAIL_PORT='.$settings_options['smtp_port']['value']."\n";
                    elseif(in_array('MAIL_ENCRYPTION', $params)):
                        $config_file[$key] = 'MAIL_ENCRYPTION='.$settings_options['smtp_encryption']['value']."\n";
                    elseif(in_array('MAIL_USERNAME', $params)):
                        $config_file[$key] = 'MAIL_USERNAME='.$settings_options['smtp_username']['value']."\n";
                    elseif(in_array('MAIL_PASSWORD', $params)):
                        $config_file[$key] = 'MAIL_PASSWORD='.$settings_options['smtp_password']['value']."\n";
                    endif;
                endforeach;
            endif;
            $fp = fopen(base_path('.env'), "w+");
            fwrite($fp, implode("", $config_file));
            fclose($fp);
            \Artisan::call('config:clear', ['--no-ansi' => TRUE, '--quiet' => TRUE]);
        endif;
    }

    private function makePngFavicon($source) {

        if(!\File::isDirectory(public_path('theme/favicon'))):
            \File::makeDirectory(public_path('theme/favicon', 0754, TRUE, TRUE));
        endif;
        \ImagineController::open($source)->resize(57, 57)->save(public_path('theme/favicon/apple-touch-icon-57x57.png'));
        \ImagineController::open($source)->resize(60, 60)->save(public_path('theme/favicon/apple-touch-icon-60x60.png'));
        \ImagineController::open($source)->resize(72, 72)->save(public_path('theme/favicon/apple-touch-icon-72x72.png'));
        \ImagineController::open($source)->resize(76, 76)->save(public_path('theme/favicon/apple-touch-icon-76x76.png'));
        \ImagineController::open($source)->resize(114, 114)->save(public_path('theme/favicon/apple-touch-icon-114x114.png'));
        \ImagineController::open($source)->resize(120, 120)->save(public_path('theme/favicon/apple-touch-icon-120x120.png'));
        \ImagineController::open($source)->resize(144, 144)->save(public_path('theme/favicon/apple-touch-icon-144x144.png'));
        \ImagineController::open($source)->resize(152, 152)->save(public_path('theme/favicon/apple-touch-icon-152x152.png'));
        \ImagineController::open($source)->resize(16, 16)->save(public_path('theme/favicon/favicon-16x16.png'));
        \ImagineController::open($source)->resize(32, 32)->save(public_path('theme/favicon/favicon-32x32.png'));
        \ImagineController::open($source)->resize(96, 96)->save(public_path('theme/favicon/favicon-96x96.png'));
        \ImagineController::open($source)->resize(128, 128)->save(public_path('theme/favicon/favicon-128.png'));
        \ImagineController::open($source)->resize(196, 196)->save(public_path('theme/favicon/favicon-196x196.png'));
        \ImagineController::open($source)->resize(70, 70)->save(public_path('theme/favicon/mstile-70x70.png'));
        \ImagineController::open($source)->resize(144, 144)->save(public_path('theme/favicon/mstile-144x144.png'));
        \ImagineController::open($source)->resize(150, 150)->save(public_path('theme/favicon/mstile-150x150.png'));
        \ImagineController::open($source)->resize(310, 310)->save(public_path('theme/favicon/mstile-310x310.png'));
    }

    private function makeIcoFavicon($source) {

        \ImagineController::save_ico($source, public_path('favicon.ico'));
    }
}