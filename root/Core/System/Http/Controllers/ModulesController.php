<?php
namespace STALKER_CMS\Core\System\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Vendor\Models\Packages;

/**
 * Class ModulesController
 * @package STALKER_CMS\Core\System\Http\Controllers
 */
class ModulesController extends ModuleController {

    /**
     * ModulesController constructor.
     */
    public function __construct() {

        \PermissionsController::allowPermission('core_system', 'modules');
        $this->middleware('auth');
    }

    /**
     * Список доступных модулей
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        $packages = new Collection();
        foreach(Packages::orderBy('required', 'DESC')->orderBy('order')->get() as $package):
            $packages[$package->slug] = $package;
        endforeach;
        return view('core_system_views::modules.index', compact('packages'));
    }

    /**
     * Повторная публикация пакета
     * @param \Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function rebuild(\Request $request) {

        $redirect_route = 'core.system.modules.index';
        if($request::has('redirect_route')):
            $redirect_route = $request::input('redirect_route');
        endif;
        foreach(Packages::whereEnabled(TRUE)->get() as $package):
            \PackagesController::publishesPackageAssets($package);
        endforeach;
        \Artisan::call('config:cache', ['--no-ansi' => TRUE, '--quiet' => TRUE]);
        return redirect()->route($redirect_route);
    }

    /**
     * Последовательность загрузки модулей
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function boot() {

        $packages = Packages::getNestedPackages();
        return view('core_system_views::modules.boot', compact('packages'));
    }

    /**
     * Отключение модулей
     * Установка и включение модулей
     * @return \Illuminate\Http\JsonResponse
     */
    public function update() {

        $request = \RequestController::isAJAX()->init();
        foreach(Packages::whereEnabled(TRUE)->whereRequired(FALSE)->get() as $package):
            if($request::has($package->slug) === FALSE):
                \PackagesController::disabledPackage($package);
            endif;
        endforeach;
        foreach(Packages::whereEnabled(FALSE)->whereRequired(FALSE)->get() as $package):
            if($request::has($package->slug)):
                if(empty($package->install_path)):
                    \PackagesController::installPackage($package);
                else:
                    \PackagesController::enabledPackage($package);
                endif;
            endif;
        endforeach;
        return \ResponseController::success(200)->redirect(route('core.system.modules.rebuild'))->json();
    }

    /**
     * Выключение модуля
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy() {

        $request = \RequestController::isAJAX()->init();
        if($package = Packages::whereId($request::get('package'))->whereEnabled(TRUE)->whereRequired(FALSE)->first()):
            \PackagesController::disabledPackage($package);
            return \ResponseController::success(200)->redirect(route('core.system.modules.index'))->json();
        endif;
        return \ResponseController::error(403)->json();
    }

    /**
     * Сохранение последовательности загрузки модулей
     * @return \Illuminate\Http\JsonResponse
     */
    public function bootUpdate() {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, ['elements' => 'required'])):
            foreach(explode(',', $request::input('elements')) as $index => $package_slug):
                Packages::whereSlug($package_slug)->update(['order' => $index + 1]);
            endforeach;
            return \ResponseController::success(202)->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Список доступных пакетов для обновления
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updates() {

        $packages = Packages::orderBy('order')->get();
        $solutions = [];
        if($solutions_packages = include_once(realpath(base_path('root/Core/System/Config/solutions.php')))):
            foreach($solutions_packages as $module => $solutions_list):
                foreach($solutions_list['solutions'] as $solution_slug => $solution):
                    if(in_array($solution_slug, array_pluck($packages, 'slug'))):
                        $solutions[] = array_first($packages, function($key, $value) use ($solution_slug) {

                            return $value->slug == $solution_slug;
                        });
                    endif;
                endforeach;
            endforeach;
        endif;
        $packages = Packages::where('slug', 'like', 'core_%')->orderBy('order')->get();
        $solutions = collect($solutions);
        return view('core_system_views::modules.updates', compact('packages', 'solutions'));
    }

    /**
     * Обновление пакетов ядра системы
     */
    public function getCoreUpdates() {

        $request = \RequestController::init();
        if(!$request::has('packages')):
            return redirect()->to(route('core.system.modules.updates').'?status=2102');
        endif;
        $composer = settings(['core_system', 'settings', 'composer']);
        $php = settings(['core_system', 'settings', 'php']);
        if(\File::exists($composer)):
            try {
                if(!\File::exists(storage_path('app/updates'))):
                    \File::makeDirectory(storage_path('app/updates'), 0754);
                else:
                    \File::deleteDirectory(storage_path('app/updates'));
                    \File::makeDirectory(storage_path('app/updates'), 0754);
                endif;
                chdir(storage_path('app/updates'));
                if(substr(php_uname(), 0, 7) == "Windows"):
                    shell_exec(trim($php.' '.$composer.' self-update'));
                    shell_exec(trim($php.' '.$composer.' --prefer-dist --no-progress --no-ansi require "grphm/stalker_updates_rc":"@dev"'));
                else:
                    shell_exec('COMPOSER_HOME="'.storage_path('app/updates').'" '.trim($php.' '.$composer).' self-update');
                    shell_exec('COMPOSER_HOME="'.storage_path('app/updates').'" '.trim($php.' '.$composer).' --prefer-dist --no-progress --no-ansi require "grphm/stalker_updates_rc":"@dev"');
                endif;
                if(\File::exists(storage_path('app/updates/vendor/grphm/stalker_updates_rc/public'))):
                    \File::copyDirectory(storage_path('app/updates/vendor/grphm/stalker_updates_rc/public'), public_path());
                endif;
                if(\File::exists(storage_path('app/updates/vendor/grphm/stalker_updates_rc/root'))):
                    $root_directory = storage_path('app/updates/vendor/grphm/stalker_updates_rc/root');
                    foreach(Packages::orderBy('order')->whereIn('id', $request::input('packages'))->get() as $package):
                        if(!empty($package->install_path) && \File::isDirectory($root_directory.'/'.$package->install_path)):
                            \File::copyDirectory($root_directory.'/'.$package->install_path, base_path('root'.'/'.$package->install_path));
                        endif;
                        \File::copyDirectory($root_directory.'/Packages', base_path('root/Packages'));
                        \File::copyDirectory($root_directory.'/Solutions', base_path('root/Solutions'));
                        \File::copyDirectory($root_directory.'/Vendor', base_path('root/Vendor'));
                    endforeach;
                endif;
                \File::deleteDirectory(storage_path('app/updates'));
                return redirect()->to(route('core.system.modules.updates').'?status=200&core='.implode(',', $request::input('packages')));
            } catch(\Exception $exception) {
                return redirect()->to(route('core.system.modules.updates').'?status=500');
            }
        else:
            return redirect()->to(route('core.system.modules.updates').'?status=2404');
        endif;
    }

    /**
     * Обновление готовых решений системы
     */
    public function getSolutionsUpdates() {

        $request = \RequestController::init();
        if(!$request::has('packages')):
            return redirect()->to(route('core.system.modules.updates').'?status=2102');
        endif;
        $solutions = [];
        if($solutions_packages = include_once(realpath(base_path('root/Core/System/Config/solutions.php')))):
            foreach($solutions_packages as $module => $solutions_list):
                foreach($solutions_list['solutions'] as $solution_slug => $solution):
                    $solutions[$solution_slug] = $solution;
                endforeach;
            endforeach;
        endif;
        $composer = settings(['core_system', 'settings', 'composer']);
        $php = settings(['core_system', 'settings', 'php']);
        if(\File::exists($composer)):
            if(!\File::exists(storage_path('app/updates'))):
                \File::makeDirectory(storage_path('app/updates'), 0754);
            else:
                \File::deleteDirectory(storage_path('app/updates'));
                \File::makeDirectory(storage_path('app/updates'), 0754);
            endif;
            chdir(storage_path('app/updates'));
            foreach(Packages::orderBy('order')->whereIn('id', $request::input('packages'))->get() as $package):
                if(isset($solutions[$package->slug])):
                    $repository = $solutions[$package->slug]['composer'];
                    $directory = $solutions[$package->slug]['directory'];
                    $repository_group = $solutions[$package->slug]['repository_group'];
                    $repository_name = $solutions[$package->slug]['repository_name'];
                    try {

                        if(substr(php_uname(), 0, 7) == "Windows"):
                            shell_exec(trim($php.' '.$composer.' self-update'));
                            shell_exec(trim($php.' '.$composer.' --prefer-dist --no-progress --no-ansi require '.$repository));
                        else:
                            shell_exec('COMPOSER_HOME="'.storage_path('app/updates').'" '.trim($php.' '.$composer).' self-update');
                            shell_exec('COMPOSER_HOME="'.storage_path('app/updates').'" '.trim($php.' '.$composer).' --prefer-dist --no-progress --no-ansi require '.$repository);
                        endif;
                        if(\File::exists(storage_path('app/updates/vendor/'.$repository_group.'/'.$repository_name.'/'.$directory))):
                            \File::copyDirectory(storage_path('app/updates/vendor/'.$repository_group.'/'.$repository_name.'/'.$directory), base_path('root/Solutions/'.$directory));
                        endif;
                    } catch(\Exception $exception) {
                        return redirect()->to(route('core.system.modules.updates').'?status=500');
                    }
                endif;
            endforeach;
            \File::deleteDirectory(storage_path('app/updates'));
            return redirect()->to(route('core.system.modules.updates').'?status=200&solutions='.implode(',', $request::input('packages')));
        else:
            return redirect()->to(route('core.system.modules.updates').'?status=2404');
        endif;
    }
}