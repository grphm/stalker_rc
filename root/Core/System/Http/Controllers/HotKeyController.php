<?php
namespace STALKER_CMS\Core\System\Http\Controllers;

/**
 * Контроллер гарячих клавиш
 * Class HotKeyController
 * @package STALKER_CMS\Core\System\Http\Controllers
 */
class HotKeyController extends ModuleController {

    public function quote() {

        \RequestController::isAJAX()->init();
        $quotes = require_once(realpath(__DIR__.'/../../Config/quotations.php'));
        $index = rand(0, count($quotes));
        if(isset($quotes[$index])):
            return \ResponseController::success(201)->set('responseText', $quotes[$index])->json();
        else:
            return \ResponseController::error(0)->json();
        endif;
    }

    public function version() {

        \RequestController::isAJAX()->init();
        return \ResponseController::success(201)->set('responseText', config('app.application_version'))->json();
    }
}