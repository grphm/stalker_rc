<?php
namespace STALKER_CMS\Core\System\Http\Controllers;

/**
 * Контроллер Dashboard пользователя
 * Class DashboardController
 * @package STALKER_CMS\Core\System\Http\Controllers
 */
class DashboardController extends ModuleController {

    /**
     * DashboardController constructor.
     */
    public function __construct() {

        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        $template = \Auth::user()->group->dashboard;
        $slug = \Auth::user()->group->slug;
        if(view()->exists("application_views::'. $slug . '.' . $template")):
            return view("core_system_views::'. $slug . '.' . $template");
        else:
            $dashboard_content = view("core_system_views::dashboards.sketch")->render();;
            if(view()->exists("home_views::packages.core_system.dashboards.$template")):
                $dashboard_content = view("home_views::packages.core_system.dashboards.$template")->render();
            endif;
            return view('core_system_views::dashboards.admin', compact('dashboard_content'));
        endif;
    }

    /**
     * Редактирование шаблон dashboard
     */
    public function editTemplate() {

        \RequestController::isAJAX()->init();
        $html = '';
        $status_code = 204;
        $dashboard_template = \Auth::user()->group->dashboard;
        if(view()->exists("home_views::packages.core_system.dashboards/".$dashboard_template)):
            $html = \File::get(base_path('home/Resources/Views/packages/core_system/dashboards/'.$dashboard_template.'.blade.php'));
            $status_code = 200;
        else:
            if(!\File::isDirectory(base_path('home/Resources/Views/packages/core_system/dashboards'))):
                \File::makeDirectory(base_path('home/Resources/Views/packages/core_system/dashboards'), 0755, TRUE, TRUE);
            endif;
            if(\File::copy(base_path('root/Core/System/Resources/Views/dashboards/sketch.blade.php'), base_path('home/Resources/Views/packages/core_system/dashboards/'.$dashboard_template.'.blade.php'))):
                $html = \File::get(base_path('home/Resources/Views/packages/core_system/dashboards/'.$dashboard_template.'.blade.php'));
                $status_code = 201;
            else:
                return \ResponseController::error(2611)->json();
            endif;
        endif;
        ob_start();
        ?>
        <div class="row p-l-10 p-b-20">
            <div class="col-sm-12">
                <button type="submit" id="dashboard-template-save" class="btn btn-primary btn-sm m-t-25 waves-effect waves-effect">
                    <i class="fa fa-save"></i>
                    <span class="btn-text">
                        <?= array_translate(['ru' => 'Сохранить', 'en' => 'Save', 'es' => 'Salvar']); ?>
                    </span>
                </button>
            </div>
        </div>
        <?php
        $button = ob_get_clean();
        return \ResponseController::success($status_code)->set('html', $html)->set('button', $button)->json();
    }

    /**
     * Сохранение шаблон dashboard
     */
    public function updateTemplate() {

        $request = \RequestController::isAJAX()->trim_spaces()->get();
        if(\ValidatorController::passes($request, ['content' => 'required'])):
            $dashboard_template = \Auth::user()->group->dashboard;
            $template = base_path('home/Resources/Views/packages/core_system/dashboards/'.$dashboard_template.'.blade.php');
            if(\File::exists($template)):
                \File::put($template, $request::input('content'));
                return \ResponseController::success(202)->redirect(route('dashboard'))->json();
            else:
                return \ResponseController::error(2611)->json();
            endif;
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }
}
