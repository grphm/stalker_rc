<?php
\Route::group(['prefix' => 'admin', 'middleware' => 'secure'], function() {

    \Route::post('/get/quote', ['uses' => 'HotKeyController@quote']);
    \Route::post('/get/version', ['uses' => 'HotKeyController@version']);
});
###########################################################################################
#Роуты для модуля Настройки
###########################################################################################
\Route::group(['prefix' => 'admin/system/settings', 'middleware' => 'secure'], function() {

    \Route::get('/', ['as' => 'core.system.settings.index', 'uses' => 'SettingsController@index']);
    \Route::post('update', ['as' => 'core.system.settings.update', 'uses' => 'SettingsController@update']);
    \Route::get('/rebuild', ['as' => 'core.system.settings.rebuild', 'uses' => 'SettingsController@rebuild']);
    \Route::get('/cache', ['as' => 'core.system.settings.cache.create', 'uses' => 'SettingsController@createConfigCash']);
    \Route::get('/clear', ['as' => 'core.system.settings.cache.clear', 'uses' => 'SettingsController@clearConfigCash']);
    \Route::get('/phpInformation', ['as' => 'core.system.settings.php', 'uses' => 'SettingsController@phpInformation']);
});
\Route::group(['prefix' => 'admin', 'middleware' => 'secure'], function() {

    \Route::get('settings', ['as' => 'cms.settings.index', 'uses' => 'SettingsController@settings']);
    \Route::post('settings', ['as' => 'cms.settings.update', 'uses' => 'SettingsController@settingsStore']);
});
###########################################################################################
#Роуты для модуля Модули
###########################################################################################
\Route::group(['prefix' => 'admin/system/modules', 'middleware' => 'secure'], function() {

    \Route::get('/', ['as' => 'core.system.modules.index', 'uses' => 'ModulesController@index']);
    \Route::get('/update', ['as' => 'core.system.modules.updates', 'uses' => 'ModulesController@updates']);
    \Route::post('/core/update', ['as' => 'core.system.modules.core_updates', 'uses' => 'ModulesController@getCoreUpdates']);
    \Route::post('/solutions/update', ['as' => 'core.system.modules.solutions_updates', 'uses' => 'ModulesController@getSolutionsUpdates']);
    \Route::get('/rebuild', ['as' => 'core.system.modules.rebuild', 'uses' => 'ModulesController@rebuild']);
    \Route::post('/enable', ['as' => 'core.system.modules.update', 'uses' => 'ModulesController@update']);
    \Route::delete('/disable', ['as' => 'core.system.modules.disable', 'uses' => 'ModulesController@destroy']);
    \Route::get('/boot', ['as' => 'core.system.modules.boot', 'uses' => 'ModulesController@boot']);
    \Route::post('/boot', ['as' => 'core.system.modules.boot.update', 'uses' => 'ModulesController@bootUpdate']);
});
###########################################################################################
#Роуты для подмодуля Решения
###########################################################################################
\Route::group(['prefix' => 'admin'.'/system/modules/solutions', 'middleware' => 'secure'], function() {

    \Route::get('/', ['as' => 'core.system.modules.solutions.index', 'uses' => 'ModulesSolutionsController@index']);
    \Route::post('enable', ['as' => 'core.system.modules.solutions.enable', 'uses' => 'ModulesSolutionsController@enable']);
    \Route::delete('disable', ['as' => 'core.system.modules.solutions.disable', 'uses' => 'ModulesSolutionsController@disable']);
});
###########################################################################################
#Роуты для модуля Группы
###########################################################################################
\Route::group(['prefix' => 'admin/system', 'middleware' => 'secure'], function() {

    \Route::get('groups/{group_id}/accesses', ['as' => 'core.system.groups.accesses-index', 'uses' => 'PermissionsController@index'])->where(['id' => '[0-9]+']);
    \Route::put('groups/{group_id}/accesses', ['as' => 'core.system.groups.accesses-update', 'uses' => 'PermissionsController@update'])->where(['id' => '[0-9]+']);
    \Route::resource('groups', 'GroupsController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'core.system.groups.index',
                'create' => 'core.system.groups.create',
                'store' => 'core.system.groups.store',
                'edit' => 'core.system.groups.edit',
                'update' => 'core.system.groups.update',
                'destroy' => 'core.system.groups.destroy',
            ]
        ]
    );
});
###########################################################################################
#Роуты для модуля Пользователи
###########################################################################################
\Route::group(['prefix' => 'admin/system', 'middleware' => 'secure'], function() {

    \Route::resource('users', 'UsersController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'core.system.users.index',
                'create' => 'core.system.users.create',
                'store' => 'core.system.users.store',
                'edit' => 'core.system.users.edit',
                'update' => 'core.system.users.update',
                'destroy' => 'core.system.users.destroy',
            ]
        ]
    );
});
###########################################################################################
#Роуты для Dashboard
###########################################################################################
\Route::group(['prefix' => 'admin', 'middleware' => 'secure'], function() {

    \Route::get('/', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
    \Route::post('dashboard/template/edit', ['as' => 'core.dashboard.template.edit', 'uses' => 'DashboardController@editTemplate']);
    \Route::post('dashboard/template/update', ['as' => 'core.dashboard.template.update', 'uses' => 'DashboardController@updateTemplate']);
});
###########################################################################################
#Роуты для модуля стран
###########################################################################################
\Route::group(['prefix' => 'admin/system', 'middleware' => 'secure'], function() {

    \Route::post('countries/import', ['as' => 'core.system.countries.import', 'uses' => 'CountriesController@import']);
    \Route::resource('countries', 'CountriesController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'core.system.countries.index',
                'create' => 'core.system.countries.create',
                'store' => 'core.system.countries.store',
                'edit' => 'core.system.countries.edit',
                'update' => 'core.system.countries.update',
                'destroy' => 'core.system.countries.destroy',
            ]
        ]
    );
});
###########################################################################################
#Роуты для модуля городов
###########################################################################################
\Route::group(['prefix' => 'admin/system', 'middleware' => 'secure'], function() {

    \Route::post('cities/import', ['as' => 'core.system.cities.import', 'uses' => 'CitiesController@import']);
    \Route::resource('cities', 'CitiesController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'core.system.cities.index',
                'create' => 'core.system.cities.create',
                'store' => 'core.system.cities.store',
                'edit' => 'core.system.cities.edit',
                'update' => 'core.system.cities.update',
                'destroy' => 'core.system.cities.destroy',
            ]
        ]
    );
});
###########################################################################################
#Роуты для модуля языков
###########################################################################################
\Route::group(['prefix' => 'admin/system', 'middleware' => 'secure'], function() {

    \Route::resource('languages', 'LanguagesController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'core.system.languages.index',
                'create' => 'core.system.languages.create',
                'store' => 'core.system.languages.store',
                'edit' => 'core.system.languages.edit',
                'update' => 'core.system.languages.update',
                'destroy' => 'core.system.languages.destroy',
            ]
        ]
    );
});