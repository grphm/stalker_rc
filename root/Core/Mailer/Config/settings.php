<?php
return [
    'mailer' => [
        'title' => ['ru' => 'Почта', 'en' => 'Mail', 'es' => 'Correo'],
        'options' => [
            ['group_title' => ['ru' => 'Настройка отправки', 'en' => 'Sending setting', 'es' => 'Envío de ajuste']],
            'from_name' => [
                'title' => [
                    'ru' => 'Имя отправителя',
                    'en' => 'Sender name',
                    'es' => 'El nombre del remitente'
                ],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => config('mail.from.name')
            ],
            'from_email' => [
                'title' => [
                    'ru' => 'Email отправителя',
                    'en' => 'Sender email',
                    'es' => 'Remitente del correo electrónico'
                ],
                'note' => [
                    'ru' => 'Для того, чтобы письма доходили до адресата необходимо для указанного доменного имени (после @) прописать DKIM и SPF записи в DNS',
                    'en' => 'To ensure delivery of your emails you should fill DKIM and SPF records in your domain DNS zone',
                    'es' => 'Para asegurar la entrega de mensajes de correo electrónico que usted debe llenar los registros DKIM y SPF en la zona DNS de dominio'
                ],
                'type' => 'text',
                'value' => config('mail.from.address')
            ],
            ['group_title' => ['ru' => 'Обратная связь', 'en' => 'Feedback', 'es' => 'Realimentación']],
            'feedback_subject' => [
                'title' => [
                    'ru' => 'Тема письма',
                    'en' => 'Letter subject',
                    'es' => 'Sujeto'
                ],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => 'Feedback'
            ],
            'feedback_emails' => [
                'title' => [
                    'ru' => 'Адреса получателей',
                    'en' => 'Recipient emails',
                    'es' => 'Mensajes de correo electrónico del destinatario'
                ],
                'note' => [
                    'ru' => 'Перечислить без пробелов через запятую',
                    'en' => 'List without spaces, separated by commas',
                    'es' => 'Lista sin espacios, separados por comas'
                ],
                'type' => 'text',
                'value' => 'feedback@grapheme.ru'
            ],
            ['group_title' => ['ru' => 'Настройки SMTP-сервера', 'en' => 'SMTP-server settings', 'es' => 'Configuración del servidor SMTP']],
            'smtp_host' => [
                'title' => [
                    'ru' => 'Host',
                    'en' => 'Host',
                    'es' => 'Host'
                ],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => config('mail.host')
            ],
            'smtp_port' => [
                'title' => [
                    'ru' => 'Порт',
                    'en' => 'Port',
                    'es' => 'Port'
                ],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => config('mail.port')
            ],
            'smtp_encryption' => [
                'title' => [
                    'ru' => 'Шифрование',
                    'en' => 'Encryption',
                    'es' => 'Entsryption'
                ],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => config('mail.encryption')
            ],
            'smtp_username' => [
                'title' => [
                    'ru' => 'Пользователь',
                    'en' => 'User name',
                    'es' => 'Nombre de usuario'
                ],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => config('mail.username')
            ],
            'smtp_password' => [
                'title' => [
                    'ru' => 'Пароль',
                    'en' => 'Password',
                    'es' => 'Contraseña'
                ],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => config('mail.password')
            ]
        ]
    ]
];