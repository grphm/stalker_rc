<?php
return [
    'package_name' => 'core_mailer',
    'package_title' => ['ru' => 'Почтовый модуль', 'en' => 'Mailing module', 'es' => 'Módulo de correo'],
    'package_icon' => 'zmdi zmdi-mail-send',
    'relations' => ['core_content'],
    'version' => [
        'ver' => 1.0,
        'date' => '15.04.2016'
    ]
];