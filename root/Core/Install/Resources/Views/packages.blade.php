@extends('root_views::layouts.install')
@section('title', trans('core_install_lang::packages.page_title'))
@section('description', trans('core_install_lang::packages.page_description'))
@section('content')
    <div class="login-content">
        <div class="lc-block toggled" id="l-login">
            <div class="lcb-form p-30">
                <div class="list-group">
                    <div class="clearfix p-0 m-h-30">
                        <div class="ah-label hidden-xs">@lang('core_install_lang::packages.form.title')</div>
                    </div>
                    <div class="list-group-item media m-t-0 m-b-10">
                        <div class="checkbox pull-left m-l-0 p-r-0">
                            <label>
                                <input class="js-check-all" autocomplete="off" type="checkbox">
                                <i class="input-helper"></i>
                            </label>
                        </div>
                        <div class="media-body text-left m-t-3">
                            <div class="lgi-heading c-blue">@lang('core_install_lang::packages.form.check_all')</div>
                        </div>
                    </div>
                    {!! Form::open(['route' => 'install.config.packages', 'class' => 'form-validate', 'id' => 'config-packages-form']) !!}
                    @foreach($packages as $package)
                        <div class="list-group-item media m-t-0 p-t-0">
                            <div class="checkbox pull-left m-l-0 p-r-0">
                                <label>
                                    {!! Form::checkbox('MODULE[' . $package->slug . ']', TRUE, FALSE, ['autocomplete' => 'off']) !!}
                                    <i class="input-helper"></i>
                                </label>
                            </div>
                            <div class="media-body text-left">
                                <div class="lgi-heading">{!! $package->title !!}</div>
                                @if(!empty($package->description))
                                    <small class="lgi-text">{{ $package->description }}</small>
                                @endif
                            </div>
                        </div>
                    @endforeach
                    {!! Form::button('<i class="zmdi zmdi-arrow-forward"></i>', ['type' => 'submit', 'class' => 'btn btn-login btn-success btn-float waves-effect waves-circle waves-float', 'autocomplete' => 'off']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts_after')
    <script>
        $(".js-check-all").click(function () {
            var prop = false;
            if ($(this).is(':checked')) {
                prop = true;
            }
            $("#config-packages-form input[type='checkbox']").prop('checked', prop);
        });
    </script>
@stop