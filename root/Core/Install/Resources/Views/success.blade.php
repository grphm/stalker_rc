@extends('root_views::layouts.errors')
@section('title', trans('core_install_lang::success.page_title'))
@section('description', trans('core_install_lang::success.page_description'))
@section('content')
    <div class="four-zero">
        <div class="fz-block">
            <h2>@lang('core_install_lang::success.title')</h2>
            <small class="f-20">@lang('core_install_lang::success.description')</small>
            <div class="fzb-links">
                <a href="{!! url('/') !!}">
                    <i class="zmdi zmdi-home"></i>
                </a>
                <a href="{!! Route::getRoutes()->hasNamedRoute('auth.login.index') ? URL::route('auth.login.index') : '/login' !!}">
                    <i class="zmdi zmdi-key"></i>
                </a>
            </div>
        </div>
    </div>
@stop