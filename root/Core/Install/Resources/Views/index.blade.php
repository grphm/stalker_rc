@extends('root_views::layouts.install')
@section('title', trans('core_install_lang::index.page_title'))
@section('description', trans('core_install_lang::index.page_description'))
@section('content')
    <div class="login-content">
        <div class="lc-block toggled" id="l-login">
            <div class="lcb-form">
                <div class="list-group lg-odd-black">
                    <div class="clearfix p-0 m-h-30">
                        <div class="ah-label hidden-xs">@lang('core_install_lang::index.form.title')</div>
                    </div>
                </div>
                {!! Form::open(['route' => 'install.config.db', 'class' => 'form-validate', 'id' => 'config-db-form']) !!}
                <div class="p-t-10 p-r-10 p-l-30">
                    <div class="form-group">
                        <p class="f-13 m-0 m-t-10 c-gray pull-left">
                            <i class="zmdi zmdi-translate"></i> @lang('core_install_lang::index.form.APP_LOCALE')
                        </p>
                        {!! Form::select('APP_LOCALE', config('core_install::config.languages'), \App::getLocale(), ['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            <input type="text" class="input-sm form-control fg-input" name="DB_DATABASE">
                            <label class="fg-label">
                                <i class="zmdi zmdi-dns"></i> @lang('core_install_lang::index.form.DB_DATABASE')
                            </label>
                        </div>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            <input type="text" class="input-sm form-control fg-input" name="DB_USERNAME">
                            <label class="fg-label">
                                <i class="zmdi zmdi-account"></i> @lang('core_install_lang::index.form.DB_USERNAME')
                            </label>
                        </div>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            <input type="password" class="input-sm form-control fg-input" name="DB_PASSWORD">
                            <label class="fg-label">
                                <i class="zmdi zmdi-key"></i> @lang('core_install_lang::index.form.DB_PASSWORD')
                            </label>
                        </div>
                    </div>
                </div>
                {!! Form::button('<i class="zmdi zmdi-arrow-forward"></i>', ['type' => 'submit', 'class' => 'btn btn-login btn-success btn-float waves-effect waves-circle waves-float', 'autocomplete' => 'off']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop