<?php
namespace STALKER_CMS\Core\Auth\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Фасад контроллера установки CMS
 * Class InstallController
 * @package STALKER_CMS\Core\Auth\Facades
 */
class InstallController extends Facade {

    protected static function getFacadeAccessor() {

        return 'InstallController';
    }
}