<?php
return [
    'package_name' => 'core_open_graph',
    'package_title' => ['ru' => 'Модуль Open Graph', 'en' => 'Open Graph module', 'es' => 'Módulo de Open Graph'],
    'package_icon' => 'zmdi zmdi-share',
    'relations' => [],
    'package_description' => [
        'ru' => 'Работа с социальными сетями',
        'en' => 'Working with social networks',
        'es' => 'El trabajo con redes sociales'
    ],
    'version' => [
        'ver' => 1.0,
        'date' => '15.04.2016'
    ]
];
