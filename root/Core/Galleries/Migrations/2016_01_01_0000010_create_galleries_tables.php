<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGalleriesTables extends Migration {

    public function up() {

        Schema::create('galleries_galleries', function(Blueprint $table) {

            $table->increments('id');
            $table->string('locale', 10)->nullable()->index();
            $table->smallInteger('template_id', FALSE, TRUE)->nullable()->index();
            $table->string('slug', 50)->nullable()->index();
            $table->string('title', 250)->nullable();
            $table->text('description')->nullable();
            $table->integer('wight_thumbnail', FALSE, TRUE)->default(200)->nullable();
            $table->integer('height_thumbnail', FALSE, TRUE)->default(200)->nullable();
            $table->timestamps();
        });
    }

    public function down() {

        Schema::dropIfExists('galleries_galleries');
    }
}

