<?php
namespace STALKER_CMS\Core\Galleries\Models;

use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

class Gallery extends BaseModel implements ModelInterface {

    use ModelTrait;
    protected $table = 'galleries_galleries';
    protected $fillable = ['locale', 'slug', 'template_id', 'title', 'description', 'wight_thumbnail', 'height_thumbnail', 'created_at', 'updated_at'];
    protected $guarded = [];

    public function insert($request) {

        $this->locale = \App::getLocale();
        $this->slug = $request::input('slug');
        $this->template_id = $request::input('template_id');
        $this->title = $request::input('title');
        $this->description = $request::input('description');
        $this->wight_thumbnail = $request::input('wight_thumbnail');
        $this->height_thumbnail = $request::input('height_thumbnail');
        $this->save();
        return $this;
    }

    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->template_id = $request::input('template_id');
        $model->title = $request::input('title');
        $model->description = $request::input('description');
        $model->wight_thumbnail = $request::input('wight_thumbnail');
        $model->height_thumbnail = $request::input('height_thumbnail');
        $model->save();
        $model->touch();
        return $model;
    }

    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function photos() {

        return $this->hasMany('\STALKER_CMS\Core\Galleries\Models\Photo', 'gallery_id', 'id');
    }

    public function getPhotosCountAttribute() {

        if(!empty($this->photos)):
            return $this->photos->count()
                ? $this->photos->count().' '.\Lang::choice('изображение|изображания|изображений', $this->photos->count())
                : 'Нет загруженных изображений';
        endif;
    }

    public function getPackageIcoAttribute() {

        if(!empty($this->package)):
            return '<i class="fa '.config($this->package->slug.'::config.package_icon').'"> </i>';
        endif;
    }

    public function getPackageTitleAttribute() {

        if(!empty($this->package)):
            return $this->package->title;
        endif;
    }

    public function getTemplateSlugAttribute() {

        if(!empty($this->template)):
            if($this->template->required):
                return NULL;
            else:
                $locale_prefix = ($this->template->locale == settings(['core_system', 'settings', 'base_locale'])) ? '' : $this->template->locale.'.';
                return ', \''.$locale_prefix.substr($this->template->path, 0, -10).'\'';
            endif;
        endif;
    }

    /**
     * Шаблон галереи
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function template() {

        return $this->hasOne('\STALKER_CMS\Core\Galleries\Models\GalleryTemplate', 'id', 'template_id');
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['slug' => 'required', 'title' => 'required', 'wight_thumbnail' => 'required', 'height_thumbnail' => 'required'];
    }

    public static function getUpdateRules() {

        return ['title' => 'required', 'wight_thumbnail' => 'required', 'height_thumbnail' => 'required'];
    }
}