@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="active">
            <i class="{{ config('core_galleries::menu.icon') }}"></i> {!! array_translate(config('core_galleries::menu.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_galleries::menu.icon') }}"></i>
            {!! array_translate(config('core_galleries::menu.title')) !!}
        </h2>
    </div>
    @if(\PermissionsController::allowPermission('core_galleries', 'create', FALSE))
        @BtnAdd('core.galleries.create')
    @endif
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">
                    @lang('core_galleries_lang::galleries.list')
                </div>
                {!! Form::open(['route' => 'core.galleries.index', 'method' => 'get']) !!}
                <div class="ah-search">
                    <input type="text" name="search" placeholder="@lang('core_galleries_lang::galleries.search')" class="ahs-input">
                    <i class="ahs-close" data-ma-action="action-header-close">&times;</i>
                </div>
                {!! Form::close() !!}
                <ul class="actions">
                    <li>
                        <a href="" data-ma-action="action-header-open">
                            <i class="zmdi zmdi-search"></i>
                        </a>
                    </li>
                    @if($galleries->count())
                        <li class="dropdown">
                            <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                                <i class="zmdi zmdi-sort-amount-asc"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="{{ route('core.galleries.index', array_merge(Request::all(), ['sort_field' => 'title', 'sort_direction' => 'asc'])) }}">
                                        @lang('core_galleries_lang::galleries.sort_title')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('core.galleries.index', array_merge(Request::all(), ['sort_field' => 'created_at', 'sort_direction' => 'asc'])) }}">
                                        @lang('core_galleries_lang::galleries.sort_published')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('core.galleries.index', array_merge(Request::all(), ['sort_field' => 'updated_at', 'sort_direction' => 'asc'])) }}">
                                        @lang('core_galleries_lang::galleries.sort_updated')
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                                <i class="zmdi zmdi-sort-amount-desc"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="{{ route('core.galleries.index', array_merge(Request::all(), ['sort_field' => 'title', 'sort_direction' => 'desc'])) }}">
                                        @lang('core_galleries_lang::galleries.sort_title')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('core.galleries.index', array_merge(Request::all(), ['sort_field' => 'created_at', 'sort_direction' => 'desc'])) }}">
                                        @lang('core_galleries_lang::galleries.sort_published')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('core.galleries.index', array_merge(Request::all(), ['sort_field' => 'updated_at', 'sort_direction' => 'desc'])) }}">
                                        @lang('core_galleries_lang::galleries.sort_updated')
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @forelse($galleries as $gallery)
                    <div class="js-item-container list-group-item media">
                        <div class="pull-right">
                            <ul class="actions dropdown">
                                <a aria-expanded="true" data-toggle="dropdown" href="">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="{!! route('core.galleries.photos_index', $gallery->id) !!}">
                                            @lang('core_galleries_lang::galleries.pictures')
                                        </a>
                                    </li>
                                    @if(\PermissionsController::allowPermission('core_galleries', 'edit', FALSE))
                                        <li>
                                            <a href="{!! route('core.galleries.edit', $gallery->id) !!}">
                                                @lang('core_galleries_lang::galleries.edit')
                                            </a>
                                        </li>
                                    @endif
                                    <li>
                                        <a href="javascript:void(0);" class="js-copy-link"
                                           data-clipboard-text="{!! '@'.'Gallery(\'' . $gallery->slug . '\''.$gallery->TemplateSlug.')' !!}">
                                            @lang('core_galleries_lang::galleries.embed')
                                        </a>
                                    </li>
                                    @if(\PermissionsController::allowPermission('core_galleries', 'delete', FALSE))
                                        <li class="divider"></li>
                                        <li>
                                            <a class="c-red js-item-remove" href="">
                                                @lang('core_galleries_lang::galleries.delete.submit')
                                            </a>
                                            {!! Form::open(['route' => ['core.galleries.destroy', $gallery->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                            <button type="submit"
                                                    data-question="@lang('core_galleries_lang::galleries.delete.question') &laquo;{{ $gallery->title }}&raquo;?"
                                                    data-confirmbuttontext="@lang('core_galleries_lang::galleries.delete.confirmbuttontext')"
                                                    data-cancelbuttontext="@lang('core_galleries_lang::galleries.delete.cancelbuttontext')">
                                            </button>
                                            {!! Form::close() !!}
                                        </li>
                                    @endif
                                </ul>
                            </ul>
                        </div>
                        <div class="media-body">
                            <div class="lgi-heading">{!! $gallery->title !!}</div>
                            @if($gallery->description)
                                <small class="lgi-text">
                                    {{ \Illuminate\Support\Str::limit(strip_tags($gallery->description), 100) }}
                                </small>
                            @endif
                            <ul class="lgi-attrs">
                                <li>
                                    @lang('core_galleries_lang::galleries.slug'):
                                    {!! $gallery->slug !!}
                                </li>
                                <li>
                                    @if($gallery->photos->count())
                                        {!! $gallery->photos->count() !!}
                                        {!! Lang::choice(trans('core_galleries_lang::galleries.pictures_count'), $gallery->photos->count()) !!}
                                    @else
                                        @lang('core_galleries_lang::galleries.missing')
                                    @endif
                                </li>
                                <li>
                                    @lang('core_galleries_lang::galleries.update'):
                                    {!! $gallery->UpdatedDate !!}
                                </li>
                            </ul>
                        </div>
                    </div>
                @empty
                    <h2 class="f-16 c-gray m-l-30">@lang('core_galleries_lang::galleries.empty')</h2>
                @endforelse
                <div class="lg-pagination p-10">
                    {!! $galleries->appends(\Request::only(['search', 'sort_field', 'sort_direction']))->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop