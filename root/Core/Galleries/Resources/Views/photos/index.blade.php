@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li>
            <a href="{!! route('core.galleries.index') !!}">
                <i class="{{ config('core_galleries::menu.icon') }}"></i> {!! array_translate(config('core_galleries::menu.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-wallpaper"></i> @lang('core_galleries_lang::photos.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2><i class="zmdi zmdi-wallpaper"></i> @lang('core_galleries_lang::photos.title')</h2>
    </div>
    @if(\PermissionsController::allowPermission('core_galleries', 'images', FALSE))
        <a class="btn bgm-deeppurple m-btn btn-float waves-effect waves-circle waves-float waves-effect waves-circle waves-float js-dropzone-show z-index-max">
            <i class="zmdi zmdi-upload"></i>
        </a>
    @endif
    <div class="card hidden" id="dropzone-container">
        <div class="card-header">
            <h2>@lang('core_galleries_lang::photos.dropzone.title')
                <small>
                    @lang('core_galleries_lang::photos.dropzone.description_formats')
                    {!! str_replace(',', ', ', settings(['core_galleries', 'galleries','allowed_file_extensions'])) !!}.
                    @lang('core_galleries_lang::photos.dropzone.description_size')
                    {!! settings(['core_galleries', 'galleries','upload_max_file_size']) !!}
                    @lang('core_galleries_lang::photos.sizes.mb').
                    @lang('core_galleries_lang::photos.dropzone.parallelUploads').
                </small>
            </h2>
            <ul class="actions">
                <li class="p-r-15">
                    <a href="{!! route('core.galleries.photos_index', $gallery->id) !!}">
                        <i class="zmdi zmdi-close"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="card-body card-padding">
            {!! Form::open(['route' => ['core.galleries.photos_store', $gallery->id], 'class' => 'dropzone', 'id' => 'dropzone-upload', 'files' => TRUE]) !!}
            {!! Form::close() !!}
            <button class="btn btn-primary btn-icon-text waves-effect m-t-15 hidden" id="js-btn-dropzone-upload">
                <i class="zmdi zmdi-cloud-upload"></i> @lang('core_galleries_lang::photos.form.upload')
            </button>
        </div>
    </div>
    <div class="card">
        @if($photos->count())
            <div class="card-header">
                <div class="ah-label hidden-xs">{!! $gallery->title !!}</div>
                <small class="c-gray f-10">{!! $gallery->description !!}</small>
                <ul class="actions" style="z-index: 100;">
                    <li class="dropdown">
                        <a href="" data-toggle="dropdown" aria-expanded="true">
                            <i class="zmdi zmdi-more-vert"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="javascript:void(0);" class="js-check-all">
                                    <i class="zmdi zmdi-check-all zmdi-hc-fw"></i> @lang('core_galleries_lang::photos.select_all')
                                </a>
                            </li>
                            @if(\PermissionsController::allowPermission('core_galleries', 'images', FALSE))
                                <li class="divider"></li>
                                <li>
                                    <a class="c-red js-selected-delete js-item-remove" href="">
                                        @lang('core_galleries_lang::photos.delete_selected.submit')
                                    </a>
                                    {!! Form::open(['route' => ['core.galleries.photos_destroy', 'selected'], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                    {!! Form::hidden('gallery', $gallery->id) !!}
                                    <button type="submit"
                                            data-question="@lang('core_galleries_lang::photos.delete_selected.question')"
                                            data-confirmbuttontext="@lang('core_galleries_lang::photos.delete_selected.confirmbuttontext')"
                                            data-cancelbuttontext="@lang('core_galleries_lang::photos.delete_selected.cancelbuttontext')">
                                    </button>
                                    {!! Form::close() !!}
                                </li>
                                <li>
                                    <a class="c-red js-item-remove" href="">
                                        @lang('core_galleries_lang::photos.delete_all.submit')
                                    </a>
                                    {!! Form::open(['route' => ['core.galleries.photos_destroy', 'all'],'class' => 'all-delete-form', 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                    {!! Form::hidden('gallery', $gallery->id) !!}
                                    <button type="submit"
                                            data-question="@lang('core_galleries_lang::photos.delete_all.question')"
                                            data-confirmbuttontext="@lang('core_galleries_lang::photos.delete_all.confirmbuttontext')"
                                            data-cancelbuttontext="@lang('core_galleries_lang::photos.delete_all.cancelbuttontext')">
                                    </button>
                                    {!! Form::close() !!}
                                </li>
                            @endif
                        </ul>
                    </li>
                </ul>
            </div>
        @endif
        <div class="card-body card-padding m-h-250 p-l-0">
            @if($photos->count())
                <ul class="lightbox photos photo-nestable-list gallery-photos-list">
                    @foreach($photos as $photo)
                        <li data-src="{{ $photo->asset_path }}"
                            class="col-md-2 col-sm-4 col-xs-6 js-item-container nested-element"
                            data-element="{!! $photo->id !!}" data-title="{{ $photo->title }}"
                            data-alt="{{ $photo->alt }}"
                            style="min-height:200px; min-width: 150px;">
                            <div class="btn bgm-gray btn-xs btn-icon waves-effect waves-circle waves-float js-btn-nested-element w-25 h-25 l-h-32"
                                 style="position: absolute; top: 8px; left: 15px; z-index: 99;">
                                <i class="zmdi zmdi-swap f-20 cursor-move"></i>
                            </div>
                            <div class="lightbox-item p-item">
                                <ul class="actions photo-actions"
                                    style="position: absolute; top: 10px; right: 10px; z-index: 99;">
                                    <li class="dropdown">
                                        <div aria-expanded="false"
                                             class="btn bgm-gray btn-xs btn-icon waves-effect waves-circle waves-float w-25 h-25 l-h-32"
                                             data-toggle="dropdown" href="">
                                            <i class="zmdi zmdi-more-vert f-20"></i>
                                        </div>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li>
                                                {!! Form::checkbox('files[]', $photo->id, NULL, ['class' => 'hidden', 'autocomplete' => 'off']) !!}
                                                <a href="javascript:void(0);" class="js-check-item">
                                                    @lang('core_galleries_lang::photos.select')
                                                </a>
                                                <a href="javascript:void(0);" class="js-check-item hidden">
                                                    @lang('core_galleries_lang::photos.cancel_selection')
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#editPhotoLabels" class="js-edit-item">
                                                    @lang('core_galleries_lang::photos.edit')
                                                </a>
                                            </li>
                                            @if(\PermissionsController::allowPermission('core_galleries', 'images', FALSE))
                                                <li class="divider"></li>
                                                <li>
                                                    <a class="c-red js-item-remove" href="">
                                                        @lang('core_galleries_lang::photos.delete.submit')
                                                    </a>
                                                    {!! Form::open(['route' => ['core.galleries.photos_destroy', $photo->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                                    {!! Form::hidden('gallery', $gallery->id) !!}
                                                    <button type="submit"
                                                            data-question="@lang('core_galleries_lang::photos.delete.question') &laquo;{{ $photo->original_name }}&raquo;?"
                                                            data-confirmbuttontext="@lang('core_galleries_lang::photos.delete.confirmbuttontext')"
                                                            data-cancelbuttontext="@lang('core_galleries_lang::photos.delete.cancelbuttontext')">
                                                        @lang('core_galleries_lang::photos.delete.submit')
                                                    </button>
                                                    {!! Form::close() !!}
                                                </li>
                                            @endif
                                        </ul>
                                    </li>
                                </ul>
                                <img src="{!! $photo->asset_thumbnail_path !!}" title="{{ $photo->title }}" alt="{{ $photo->alt }}"/>
                            </div>
                        </li>
                    @endforeach
                </ul>
                <div class="clearfix"></div>
            @else
                <h2 class="f-16 c-gray m-l-30">@lang('core_galleries_lang::photos.empty')</h2>
            @endif
        </div>
    </div>
@stop
@section('modal')
    <div class="modal" id="editPhotoLabels">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                </div>
                {!! Form::open(['route' => 'core.galleries.photos_update', 'class' => 'form-validate', 'id' => 'edit-gallery-photo-form']) !!}
                {!! Form::hidden('photo_id') !!}
                <div class="modal-body">
                    <div class="form-group fg-line">
                        <label class="fg-label">@lang('core_galleries_lang::photos.modal_edit.title')</label>
                        {!! Form::text('title', NULL, ['class'=>'input-sm form-control']) !!}
                    </div>
                    <div class="form-group fg-line">
                        <label class="fg-label">@lang('core_galleries_lang::photos.modal_edit.alt_text')</label>
                        {!! Form::text('alt', NULL, ['class' => 'input-sm form-control']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect">
                        <i class="fa fa-save"></i>
                        <span class="btn-text">@lang('core_galleries_lang::photos.modal_edit.submit')</span>
                    </button>
                    <a href="javascript:void(0);" class="js-edit-cancel btn btn-danger waves-effect">
                        <span>@lang('core_galleries_lang::photos.modal_edit.cancel')</span>
                    </a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts_before')
    <script>
        $(".js-btn-nested-element").click(function (event) {
            event.stopPropagation();
            event.preventDefault();
        })
        $(".js-check-item").click(function (event) {
            var input = $(this).siblings('input:checkbox');
            $('.dropdown').removeClass('open');
            event.preventDefault();
            event.stopPropagation();
            if (input.is(':checked')) {
                input.prop("checked", false);
                input.parents(".p-item").removeClass('item-selected');
            } else {
                input.prop("checked", true);
                input.parents(".p-item").addClass('item-selected');
            }
            $(this).addClass('hidden');
            $(this).siblings('.js-check-item').removeClass('hidden');
        });
        $(".photo-actions").click(function (event) {
            event.stopPropagation();
            event.preventDefault();
            $(this).find('.form-confirm-warning').off('click');
            if ($(this).find('.dropdown').hasClass('open')) {
                $(this).find('.dropdown').removeClass('open');
            } else {
                $(this).find('.dropdown').addClass('open');
                $(this).find('.form-confirm-warning').on('click', function (event) {
                    event.preventDefault();
                    $(this).parents('.dropdown').removeClass('open');
                    BASIC.ajax = {
                        url: $(this).parents('form').attr('action'),
                        type: $(this).parents('form').find('input[name="_method"]').val(),
                        data: $(this).parents('form').formSerialize()
                    }
                    if (typeof BASIC.ajax.type == 'undefined') {
                        BASIC.ajax.type = $(this).parents('form').attr('method')
                    }
                    BASIC.ShowConfirmDialog(this);
                    $(this).find('.dropdown').removeClass('open');
                });
            }
        });

        $(".js-selected-delete").click(function () {
            var form = $(this).siblings('form');
            $(form).find('.js-dynamic-append').remove();
            $(".js-item-container input[type='checkbox']:checked").each(function (index, element) {
                $(form).append('<input type="hidden" class="js-dynamic-append" name="files[]" value="' + $(element).val() + '">');
            })
        });

        $(".selected-delete-form button[type='submit']").click(function () {
            $(".selected-delete-form .js-dynamic-append").remove();
            $(".photos input[type='checkbox']:checked").each(function (index, element) {
                $(".selected-delete-form").append('<input type="hidden" class="js-dynamic-append" name="files[]" value="' + $(element).val() + '">');
            })
        });
    </script>
@stop
@section('scripts_after')
    <script>
        $(function () {
            Dropzone.autoDiscover = false;
            var loadedThumbnail = 0;
            var GalleryDropZone = new Dropzone("#dropzone-upload", {
                parallelUploads: 50,
                addRemoveLinks: true,
                autoProcessQueue: false,
                maxFilesize: {!! settings(['core_galleries', 'galleries','upload_max_file_size']) !!},
                thumbnailWidth: {!! $gallery->wight_thumbnail !!},
                thumbnailHeight: {!! $gallery->height_thumbnail !!},
                acceptedFiles: '{!! implode(',', $allowed_types) !!}',
                dictDefaultMessage: '@lang('core_galleries_lang::photos.dropzone.dictDefaultMessage')',
                dictFallbackMessage: '@lang('core_galleries_lang::photos.dropzone.dictFallbackText')',
                dictInvalidFileType: '@lang('core_galleries_lang::photos.dropzone.dictInvalidFileType')',
                dictFileTooBig: '@lang('core_galleries_lang::photos.dropzone.dictFileTooBig')',
            });
            GalleryDropZone.on('thumbnail', function (file, thumb) {
                file.thumbnail = thumb;
                loadedThumbnail = loadedThumbnail + 1;
                if (loadedThumbnail == GalleryDropZone.files.length) {
                    $("#js-btn-dropzone-upload").removeClass('hidden');
                }
            });
            GalleryDropZone.on("addedfile", function (file, xhr, formData) {
                $("#js-btn-dropzone-upload").addClass('hidden');
            });
            GalleryDropZone.on("removedfile", function (file, xhr, formData) {
                loadedThumbnail = loadedThumbnail - 1;
                if (loadedThumbnail == 0) {
                    $("#js-btn-dropzone-upload").addClass('hidden');
                }
            });
            GalleryDropZone.on("sending", function (file, xhr, formData) {
                formData.append('_token', $("#dropzone-upload input[name='_token']").val());
                formData.append('thumbnail', file.thumbnail);
            });
            GalleryDropZone.on("queuecomplete", function (file) {
                window.location.href = "{!! route('core.galleries.photos_index', $gallery->id) !!}"
            });
            $("#js-btn-dropzone-upload").click(function () {
                $(this).attr('disabled', 'disabled');
                GalleryDropZone.processQueue();
            });
            $(".js-dropzone-show").click(function (event) {
                $("#dropzone-container").removeClass('hidden');
                $(this).addClass('hidden');
            });
            $(".js-check-all").click(function () {
                $(".photos input[type='checkbox']").prop('checked', true);
                $(".photos .p-item").addClass('item-selected');
                $(".photos .js-check-item").addClass('hidden');
                $(".photos .dropdown-menu").each(function (index, element) {
                    $(element).find('.js-check-item').eq(1).removeClass('hidden');
                });
            });
            function setImgSize(imgSrc) {
                var newImg = new Image();
                newImg.src = imgSrc;
                var height = newImg.height;
                var width = newImg.width;
                $(newImg).load(function () {
                    $(".js-preview-image-wight").html(newImg.width);
                    $(".js-preview-image-height").html(newImg.height);
                    $(".cmsPreviewOverlay").show();
                });
            }
        })
    </script>
    <script>
        $(function () {
            $(".js-edit-item").click(function (event) {
                var item_id = $(this).parents('.js-item-container').data('element');
                var item_title = $(this).parents('.js-item-container').data('title');
                var item_alt = $(this).parents('.js-item-container').data('alt');
                $("#editPhotoLabels").find('form input[name="photo_id"]').val(item_id);
                $("#editPhotoLabels").find('form input[name="alt"]').val(item_alt);
                $("#editPhotoLabels").find('form input[name="title"]').val(item_title);
                $("#editPhotoLabels").show();
            });
            $(".js-edit-cancel").click(function () {
                $("#editPhotoLabels").hide();
            });
        });
    </script>
    <script>
        var nestableList = [];
        var list = $('.photo-nestable-list').sortable({
            group: 'photo-nestable-list',
            delay: 500,
            handle: 'i.zmdi-swap',
            onDrop: function ($item, container, _super) {
                nestableList = [];
                list.sortable("serialize").get();
                $.ajax({
                    url: '{!! route('core.galleries.photos_sortable', $gallery->id) !!}',
                    type: 'POST',
                    dataType: 'json',
                    data: {elements: nestableList.join(',')},
                    beforeSend: function () {
                    },
                    success: function (response, textStatus, xhr) {
                        if (response.status == true) {
                            BASIC.notify(null, response.responseText, 'bottom', 'center', 'zmdi zmdi-notifications-none zmdi-hc-fw', 'success', 'animated flipInX', 'animated flipOutX');
                        } else {
                            BASIC.notify(null, response.errorText, 'bottom', 'center', null, 'danger', 'animated flipInX', 'animated flipOutX');
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        BASIC.notify(null, xhr.responseText, 'bottom', 'center', null, 'danger', 'animated flipInX', 'animated flipOutX');
                    }
                });
                _super($item, container);
            },
            serialize: function (parent, children, isContainer) {
                if ($(parent).hasClass('nested-element')) {
                    nestableList.push($(parent).data('element'));
                }
            }
        });
    </script>
@stop