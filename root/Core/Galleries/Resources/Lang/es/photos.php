<?php
return [
    'breadcrumb' => 'Fotos',
    'title' => 'Fotos',
    'select_all' => 'Seleccionar todo',
    'delete_selected' => [
        'question' => 'Eliminar itages selekted?',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar selección',
    ],
    'delete_all' => [
        'question' => 'Eliminar todas las imágenes?',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar todos',
    ],
    'edit' => 'Editar',
    'select' => 'Seleccionar',
    'cancel_selection' => 'Cancelar la selección',
    'delete' => [
        'question' => 'Eliminar imagen',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar',
    ],
    'empty' => 'Lista está vacía',
    'dropzone' => [
        'title' => 'Sube imágenes de la galería',
        'description_formats' => 'Formatos soportados: ',
        'description_size' => 'Carga máxima a ti: ',
        'dictDefaultMessage' => 'Arrastra los archivos aquí para subirlos',
        'dictFallbackMessage' => 'Su navegador no soporta arrastrar y soltar la carga de archivos',
        'dictFallbackText' => 'Por favor, utilice el formulario de reserva de abajo para subir sus archivos al igual que en los viejos tiempos',
        'dictInvalidFileType' => 'Tkhe filete de Dawes señala partido Tae alloved filetes Topes',
        'dictFileTooBig' => 'El archivo es demasiado grande',
        'parallelUploads' => 'No más de 50 descargas simultáneas'
    ],
    'form' => [
        'size' => 'El Tamaño',
        'type' => 'Tipo',
        'upload' => 'Subir',
        'uploading' => 'Cargar archivos',
        'done' => 'hecho'
    ],
    'modal_edit' => [
        'title' => 'Título',
        'alt_text' => 'Alt texto',
        'submit' => 'Guardar',
        'cancel' => 'Cancelar'
    ],
    'sizes' => ['mb' => 'Mb', 'kb' => 'kb', 'b' => 'b', 'undefined' => 'El tamaño no se define'],
    'mime_undefined' => 'Mime-tipo no está definido'
];