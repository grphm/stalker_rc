<?php
return [
    'breadcrumb' => 'Изображения',
    'title' => 'Изображения',
    'select_all' => 'Выбрать все',
    'delete_selected' => [
        'question' => 'Удалить выбранные изображения?',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить выбранные',
    ],
    'delete_all' => [
        'question' => 'Удалить все изображения?',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить все',
    ],
    'edit' => 'Редактировать',
    'select' => 'Выбрать',
    'cancel_selection' => 'Отменить выбор',
    'delete' => [
        'question' => 'Удалить файл',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить',
    ],
    'empty' => 'Список пустой',
    'dropzone' => [
        'title' => 'Загрузка изображений галереи',
        'description_formats' => 'Поддерживаемые форматы: ',
        'description_size' => 'Максимальный размер загружаемого файла: ',
        'dictDefaultMessage' => 'Перетащите сюда файлы для загрузки',
        'dictFallbackMessage' => 'Ваш браузер не поддерживает перетаскивания файлов для загрузки',
        'dictFallbackText' => 'Пожалуйста, используйтесь формой ниже, чтобы загружать файлы, как раньше',
        'dictInvalidFileType' => 'Файл не соответствует разрешенным типам файлов',
        'dictFileTooBig' => 'Файл слишком велик',
        'parallelUploads' => 'Не более 50 одновременных загрузок'
    ],
    'form' => [
        'size' => 'Размер',
        'type' => 'Тип',
        'upload' => 'Загрузить',
        'uploading' => 'Загрузка файла',
        'done' => 'выполнено',
    ],
    'modal_edit' => [
        'title' => 'Название',
        'alt_text' => 'Alt текст',
        'submit' => 'Сохранить',
        'cancel' => 'Отмена'
    ],
    'sizes' => ['mb' => 'Мбайт', 'kb' => 'kБайт', 'b' => 'байт', 'undefined' => 'Размер не определен'],
    'mime_undefined' => 'Mime-тип не определен'
];