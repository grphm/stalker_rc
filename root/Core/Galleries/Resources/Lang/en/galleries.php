<?php
return [
    'list' => 'List of galleries',
    'pictures' => 'Pictures',
    'edit' => 'Edit',
    'embed' => 'Embed code',
    'update' => 'Update date',
    'slug' => 'Symbolic code',
    'search' => 'Enter the name of the gallery',
    'sort_title' => 'Título',
    'sort_published' => 'Creation date',
    'sort_updated' => 'Update date',
    'delete' => [
        'question' => 'Delete gallery',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'missing' => 'Images are missing',
    'pictures_count' => 'picture|pictures|pictures',
    'empty' => 'List is empty',
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding gallery',
        'form' => [
            'title' => 'Title',
            'title_help_description' => 'For example: Main Gallery',
            'template' => 'Gallery Template',
            'description' => 'Description',
            'slug' => 'Symbolic code',
            'slug_help_description' => 'Only latin characters, underscores, dashes, at least 5 characters. <br> Example: main_gallery',
            'wight_thumbnail' => 'Fixed width thumbnail in pixels',
            'height_thumbnail' => 'Fixed height of the thumbnail in pixels',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Edit gallery',
        'form' => [
            'title' => 'Title',
            'title_help_description' => 'For example: Main Gallery',
            'template' => 'Gallery Template',
            'description' => 'Description',
            'slug' => 'Symbolic code',
            'slug_help_description' => 'Only latin characters, underscores, dashes, at least 5 characters. <br> Example: main_gallery',
            'wight_thumbnail' => 'Fixed width thumbnail in pixels',
            'height_thumbnail' => 'Fixed height of the thumbnail in pixels',
            'submit' => 'Save'
        ]
    ]
];