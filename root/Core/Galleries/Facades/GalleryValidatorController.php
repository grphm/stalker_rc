<?php
namespace STALKER_CMS\Core\Galleries\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Фасад контроллера Галерия
 * Class GalleryValidatorController
 * @package STALKER_CMS\Core\Galleries\Facades
 */
class GalleryValidatorController extends Facade {

    /**
     * @return string
     */
    protected static function getFacadeAccessor() {

        return 'GalleryValidatorController';
    }
}