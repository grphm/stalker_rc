<?php
namespace STALKER_CMS\Core\Galleries\Http\Controllers;

use STALKER_CMS\Core\Galleries\Models\Gallery;
use Illuminate\Database\Eloquent\Collection;

/**
 * Контроллер для получения галереи в гостевом интерфейсе
 * Class PublicGalleriesController
 * @package STALKER_CMS\Core\Galleries\Http\Controllers
 */
class PublicGalleriesController extends ModuleController {

    protected $model;

    /**
     * PublicGalleriesController constructor.
     */
    public function __construct() {

        $this->model = new Gallery();
    }

    /**
     * Возвращает список изображений в галереи по символьному коду
     * @param $symbolicCode
     * @return Collection
     */
    public function images($symbolicCode) {

        if($gallery = $this->model->whereSlug($symbolicCode)->whereLocale(\App::getLocale())->first()):
            return $gallery->photos()->orderBy('order')->get();
        else:
            return new Collection();
        endif;
    }

    /**
     * Возвращает информацию о галереи по символьному коду
     * @param $symbolicCode
     * @return mixed
     */
    public function gallery($symbolicCode) {

        return $this->model->whereSlug($symbolicCode)->whereLocale(\App::getLocale())->first();
    }
}