<?php
return [
    'package_name' => 'core_galleries',
    'package_title' => ['ru' => 'Модуль галерей', 'en' => 'Module galleries', 'es' => 'Galerías del módulo'],
    'package_icon' => 'zmdi zmdi-collection-image',
    'relations' => [],
    'package_description' => [
        'ru' => 'Позволяет управлять галереями',
        'en' => 'It allows you to manage galleries',
        'es' => 'Se le permite gestionar galerías'
    ],
    'version' => [
        'ver' => 1.3,
        'date' => '12.01.2017'
    ]
];