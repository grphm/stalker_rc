<?php
namespace STALKER_CMS\Vendor\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class ResponseController
 * Фасад контроллера обрабатывающего ответы от сервера
 * @package STALKER_CMS\Vendor\Facades
 */
class ResponseController extends Facade {

    protected static function getFacadeAccessor() {

        return 'ResponseController';
    }
}