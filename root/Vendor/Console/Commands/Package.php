<?php
namespace STALKER_CMS\Vendor\Console\Commands;

use Illuminate\Console\Command;
use STALKER_CMS\Vendor\Models\Packages;

/**
 * Class Package
 * Команда позволяет устанавливать или удалять пакет из CMS
 * @package STALKER_CMS\Vendor\Console\Commands
 */
class Package extends Command {

    protected $signature = 'Package {package_slug} {--delete}';
    protected $description = 'Установка или удаление пакетов';

    public function __construct() {

        parent::__construct();
    }

    public function handle() {

        if(\File::exists(base_path('bootstrap/cache/config.php'))):
            $this->error('For install/delete package, delete the cache file settings.');
            return FALSE;
        endif;
        if($this->option('delete')):
            if($this->confirm('Uninstall package. Continue? [yes|no]')):
                if($package = Packages::whereSlug($this->argument('package_slug'))->whereRequired(FALSE)->first()):
                    \PackagesController::disabledPackage($package);
                    if($solution = $this->getSolution($this->argument('package_slug'))):
                        \File::deleteDirectory(base_path('root/'.$package->install_path));
                        $package->delete();
                    endif;
                    if(file_exists(base_path('bootstrap/cache/config.php'))):
                        unlink(base_path('bootstrap/cache/config.php'));
                    endif;
                    $this->info('Uninstall package successfully');
                else:
                    $this->error('Package not found');
                    return FALSE;
                endif;
            endif;
        else:
            if($this->confirm('Install package. Continue? [yes|no]')):
                if(!$package = Packages::whereSlug($this->argument('package_slug'))->whereRequired(FALSE)->first()):
                    if(!$solution = $this->installSolution($this->argument('package_slug'))):
                        $this->error('Package not found');
                        return FALSE;
                    else:
                        $this->info('Package downloaded successfully. Run the installation process again.');
                        return FALSE;
                    endif;
                endif;
                if($package->enabled):
                    $this->info('Package already installed');
                else:
                    if(empty($package->install_path)):
                        \PackagesController::installPackage($package);
                    else:
                        \PackagesController::enabledPackage($package);
                    endif;
                    $this->info('Install package successfully');
                endif;
            endif;
        endif;
    }

    private function installSolution($package_slug) {

        if($solution_package = $this->getSolution($package_slug)):
            $composer = settings(['core_system', 'settings', 'composer']);
            $repository = $solution_package['composer'];
            $directory = $solution_package['directory'];
            $repository_group = $solution_package['repository_group'];
            $repository_name = $solution_package['repository_name'];
            $php = settings(['core_system', 'settings', 'php']);
            if(\File::exists($composer)):
                try {
                    if(!\File::exists(storage_path('app/updates'))):
                        \File::makeDirectory(storage_path('app/updates'), 0754);
                    else:
                        \File::deleteDirectory(storage_path('app/updates'));
                        \File::makeDirectory(storage_path('app/updates'), 0754);
                    endif;
                    chdir(storage_path('app/updates'));
                    if(substr(php_uname(), 0, 7) == "Windows"):
                        shell_exec(trim($php.' '.$composer.' self-update'));
                        shell_exec(trim($php.' '.$composer.' --prefer-dist --no-progress --no-ansi require '.$repository));
                    else:
                        shell_exec('COMPOSER_HOME="'.storage_path('app/updates').'" '.trim($php.' '.$composer).' self-update');
                        shell_exec('COMPOSER_HOME="'.storage_path('app/updates').'" '.trim($php.' '.$composer).' --prefer-dist --no-progress --no-ansi require '.$repository);
                    endif;
                    if(\File::exists(storage_path('app/updates/vendor/'.$repository_group.'/'.$repository_name.'/'.$directory))):
                        \File::copyDirectory(storage_path('app/updates/vendor/'.$repository_group.'/'.$repository_name.'/'.$directory), base_path('root/Solutions/'.$directory));
                    endif;
                    \File::deleteDirectory(storage_path('app/updates'));
                    if(\File::exists(base_path('root/Solutions/'.$directory))):
                        $package = new Packages();
                        $package->slug = $solution_package['package_name'];
                        $package->title = json_encode($solution_package['package_title']);
                        $package->description = json_encode($solution_package['package_description']);
                        $package->relations = implode('|', $solution_package['relations']);
                        $package->composer_config = $repository;
                        $package->order = Packages::where('order', '<', 1000)->max('order') + 1;
                        $package->save();
                        return Packages::where('id', $package->id)->first();
                    else:
                        $this->error('Failed to install package');
                        return FALSE;
                    endif;
                } catch(\Exception $exception) {
                    if(\File::exists(base_path('root/Solutions/'.$directory))):
                        \File::deleteDirectory(base_path('root/Solutions/'.$directory));
                    endif;
                    Packages::whereSlug($package_slug)->delete();
                    $this->error('Failed to install package');
                    return FALSE;
                }
            else:
                $this->error('Composer not found');
                return FALSE;
            endif;
        endif;
        return FALSE;
    }

    private function getSolution($package_slug) {

        $solutions_packages = include_once(realpath(base_path('root/Core/System/Config/solutions.php')));
        foreach($solutions_packages as $section => $solutions_package):
            if(isset($solutions_packages[$section]['solutions'][$package_slug])):
                return $solutions_packages[$section]['solutions'][$package_slug];
            endif;
        endforeach;
        return FALSE;
    }
}
