<?php
namespace STALKER_CMS\Vendor\Console\Commands;

use Illuminate\Console\Command;

/**
 * Class Uninstall
 * Команда удаляет отменяет установку CMS
 * Внимание! База Данных полностью очищается
 * @package STALKER_CMS\Vendor\Console\Commands
 */
class Uninstall extends Command {

    protected $signature = 'Uninstall {--full}';
    protected $description = 'Отмена установки';

    public function __construct() {

        parent::__construct();
    }

    public function handle() {

        if($this->confirm('Uninstall STALKER CMS. Continue? [yes|no]')) {
            if(!file_exists(base_path('.env'))):
                $this->info('File .env not found. Uninstall is not possible.');
                return FALSE;
            endif;
            $database = config('database.connections.'.config('database.default').'.database');
            $table_index_name = 'Tables_in_'.$database;
            $tables = \DB::select('show full tables where Table_Type != "VIEW"');
            $views = \DB::select('show full tables where Table_Type = "VIEW"');
            if(count($tables)):
                foreach($tables as $table):
                    $table = json_decode(json_encode($table), TRUE);
                    if(is_array($table) && isset($table[$table_index_name])):
                        \Schema::drop($table[$table_index_name]);
                    endif;
                endforeach;
            endif;
            if(count($views)):
                foreach($views as $view):
                    $view = json_decode(json_encode($view), TRUE);
                    if(is_array($view) && isset($view[$table_index_name])):
                        \DB::statement('DROP VIEW '.$view[$table_index_name]);
                    endif;
                endforeach;
            endif;
            if($this->option('full')):
                $this->full();
            endif;
            if(file_exists(base_path('.env'))):
                unlink(base_path('.env'));
            endif;
            if(file_exists(base_path('bootstrap/cache/config.php'))):
                unlink(base_path('bootstrap/cache/config.php'));
            endif;
            if(file_exists(base_path('bootstrap/cache/services.php'))):
                unlink(base_path('bootstrap/cache/services.php'));
            endif;
            if(file_exists(storage_path('logs/laravel.log'))):
                unlink(storage_path('logs/laravel.log'));
            endif;
            $cachedViewsDirectory = storage_path('framework/views');
            array_map("unlink", glob("$cachedViewsDirectory/*.php"));
            setcookie("stalker_cms", "", time() - 3600);
            $this->info('--------------------------------');
            $this->info('Uninstall completed successfully');
        }
    }

    private function full() {

        \File::deleteDirectory(public_path('theme/favicon'));
        \File::deleteDirectory(public_path('theme/fonts'));
        \File::deleteDirectory(public_path('theme/images'));
        \File::deleteDirectory(public_path('theme/scripts'));
        \File::deleteDirectory(public_path('theme/styles'));
        $this->info('Uninstall theme files successfully');
        array_map("unlink", glob(base_path('home/Resources/Lang/en/*.php')));
        array_map("unlink", glob(base_path('home/Resources/Lang/es/*.php')));
        array_map("unlink", glob(base_path('home/Resources/Lang/ru/*.php')));
        array_map("unlink", glob(base_path('home/Resources/Mails/*.php')));
        array_map("unlink", glob(base_path('home/Resources/Views/*.php')));
        $this->info('Uninstall resources successfully');
        \Storage::deleteDirectory('avatars');
        \Storage::deleteDirectory('content');
        \Storage::deleteDirectory('dictionaries');
        \Storage::deleteDirectory('files');
        \Storage::deleteDirectory('galleries');
        \Storage::deleteDirectory('images');
        \Storage::deleteDirectory('pages');
        $this->info('Uninstall uploads files successfully');
        \File::deleteDirectory(base_path('root/Solutions/Articles'));
        \File::deleteDirectory(base_path('root/Solutions/Bugnotify'));
        \File::deleteDirectory(base_path('root/Solutions/Console'));
        \File::deleteDirectory(base_path('root/Solutions/Events'));
        \File::deleteDirectory(base_path('root/Solutions/News'));
        \File::deleteDirectory(base_path('root/Solutions/Questions'));
        \File::deleteDirectory(base_path('root/Solutions/Sliders'));
        \File::deleteDirectory(base_path('root/Solutions/SocialImageGrabber'));
        \File::deleteDirectory(base_path('root/Solutions/Subscriptions'));
        \File::deleteDirectory(public_path('core/console'));
        $this->info('Uninstall solutions successfully');
    }
}
