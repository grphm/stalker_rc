<?php
namespace STALKER_CMS\Vendor\Console\Commands;

use Illuminate\Console\Command;

/**
 * Class Update
 * Команда обновляет CMS до заданной версии
 * @package STALKER_CMS\Vendor\Console\Commands
 */
class Update extends Command {

    protected $signature = 'Update {--composer=/usr/local/bin/composer} {--release=2.0.5.2-RC1.0}';
    protected $description = 'Обновление ядра CMS';

    public function __construct() {

        parent::__construct();
    }

    public function handle() {

        $errors = FALSE;
        $composer = $this->option('composer');
        $version = $this->option('release');
        $this->info('Configuration:');
        $this->info("OS ".substr(php_uname(), 0, 7));
		if(!\File::exists($composer)):
            $composer = settings(['core_system', 'settings', 'composer']);
            if(!\File::exists($composer)):
                $this->error('Error: Composer not found!');
                $errors = TRUE;
            endif;
        endif;
        $this->info("Composer: $composer");
        $this->info("Package: grphm/stalker_update_rc");
        $this->info("Version: $version");
        if(!$errors && $this->confirm('Execute the update STALKER CMS. Continue? [yes|no]')):
            $this->info('The upgrade process started.');
            $this->info('-----------------------------');
            try {
                if(!\File::exists(storage_path('app/updates'))):
                    \File::makeDirectory(storage_path('app/updates'), 0754);
                else:
                    \File::deleteDirectory(storage_path('app/updates'));
                    \File::makeDirectory(storage_path('app/updates'), 0754);
                endif;
                chdir(storage_path('app/updates'));
                $this->info("Composer require grphm/stalker_updates_rc:$version");
                if(substr(php_uname(), 0, 7) == "Windows"):
                    shell_exec($composer.' self-update');
                    $out = shell_exec($composer.' --prefer-dist --no-progress --no-ansi require "grphm/stalker_updates_rc":"'.$version.'" 2>&1');
                else:
                    shell_exec('COMPOSER_HOME="'.storage_path('app/updates').'" '.$composer.' self-update');
                    $out = shell_exec('COMPOSER_HOME="'.storage_path('app/updates').'" '.$composer.' --prefer-dist --no-progress --no-ansi require "grphm/stalker_updates_rc":"'.$version.'" 2>&1');
                endif;
                $this->info($out);
                if(\File::exists(storage_path('app/updates/vendor/grphm/stalker_updates_rc/public'))):
                    \File::copyDirectory(storage_path('app/updates/vendor/grphm/stalker_updates_rc/public'), public_path());
                endif;
                if(\File::exists(storage_path('app/updates/vendor/grphm/stalker_updates_rc/root'))):
                    \File::copyDirectory(storage_path('app/updates/vendor/grphm/stalker_updates_rc/root'), base_path('root'));
                endif;
                \File::deleteDirectory(storage_path('app/updates'));
                $this->info('-----------------------------');
                $this->info('Update completed successfully');
            } catch(\Exception $exception) {
                $this->info('-----------------------------');
                $this->error($exception->getMessage());
            }
        endif;
    }
}
