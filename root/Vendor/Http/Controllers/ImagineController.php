<?php
namespace STALKER_CMS\Vendor\Http\Controllers;

use STALKER_CMS\Packages\Imagine\Icon\Icon;
use STALKER_CMS\Packages\Imagine\Image\Box;
use STALKER_CMS\Packages\Imagine\Image\Point;
use STALKER_CMS\Packages\Imagine\Image\Palette\RGB;
use STALKER_CMS\Packages\Imagine\Imagick\Imagine;

class ImagineController {

    protected $imagine;
    protected $image;

    function __construct() {

        $this->imagine = new Imagine();
    }

    public function open($file_path) {

        $this->image = $this->imagine->open($file_path);
        return $this;
    }

    public function create($width, $height, $color = '#000') {

        $palette = new RGB();
        $size = new Box($width, $height);
        $color = $palette->color($color);
        $this->image = $this->imagine->create($size, $color);
        return $this;
    }

    public function watermark($watermark_path, $file_path) {

        $watermark = $this->imagine->open($watermark_path);
        $this->image = $this->imagine->open($file_path);
        $size = $this->image->getSize();
        $wSize = $watermark->getSize();
        $bottomRight = new Point($size->getWidth() - $wSize->getWidth(), $size->getHeight() - $wSize->getHeight());
        $this->image->paste($watermark, $bottomRight);
    }

    public function resize($width, $height) {

        $this->image = $this->image->resize(new Box($width, $height));
        return $this;
    }

    public function rotate($angle) {

        $this->image = $this->image->rotate($angle);
        return $this;
    }

    public function crop($point_x = 0, $point_y = 0, $width, $height) {

        $this->image = $this->image->crop(new Point($point_x, $point_y), new Box($width, $height));
        return $this;
    }

    public function save($file_path) {

        $this->image->save($file_path);
        return $this;
    }

    public function save_ico($source, $destination, $width = 32, $height = 32) {

        $ico_lib = new Icon($source, [[$width, $height]]);
        $ico_lib->save_ico($destination);
    }
}