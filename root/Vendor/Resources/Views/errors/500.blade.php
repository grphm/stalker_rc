@extends('root_views::layouts.errors')
@section('title', $message)
@section('content')
    <div class="four-zero">
        <div class="fz-block">
            <h2>{{ $code }}</h2>
            <small class="f-20">{{ $message }}</small>
            <div class="fzb-links">
                <a href="{!! url('/') !!}"><i class="zmdi zmdi-home"></i></a>
            </div>
        </div>
    </div>
@stop