<?php
/**
 * Выводит на экран данные полученные от пользователя
 */
if(!function_exists('tadi')) {
    function tadi() {

        _dd(\Request::all());
    }
}
/**
 * Выводит на экран содержимое объекта или массива
 * Останавливает дальнейшее выполнения приложения
 * @param $object
 */
if(!function_exists('tad')) {
    function tad($object) {

        if(is_object($object)):
            _dd($object->toArray());
        elseif(is_array($object)):
            _dd($object);
        elseif(is_string($object) || is_numeric($object)):
            _dd((array)$object);
        endif;
    }
}
/**
 * Выводит на экран содержимое объекта или массива
 * Не останавливает дальнейшее выполнения приложения
 * @param $object
 */
if(!function_exists('ta')) {
    function ta($object) {

        $return = $object;
        if(is_object($object)):
            $return = $object->toArray();
        elseif(is_array($object)):
            foreach($object as $o => $obj):
                $return[$o] = is_object($obj) ? $obj->toArray() : $obj;
            endforeach;
        endif;
        _d((array)$return);
    }
}
/**
 * Выводит на экран содержимое массива
 * Не останавливает дальнейшее выполнения приложения
 * @param array $array
 */
if(!function_exists('_d')) {
    function _d(array $array) {

        if(count($array)):
            echo "\n<pre style='text-align:left'>\n".print_r($array, 1)."\n</pre>\n";
        else:
            echo "\n<pre style='text-align:left'>\nПустое значение\n</pre>\n";
        endif;
    }
}
/**
 * Выводит на экран содержимое массива
 * Останавливает дальнейшее выполнения приложения
 * @param array $array
 */
if(!function_exists('_dd')) {
    function _dd(array $array) {

        _d($array);
        die;
    }
}