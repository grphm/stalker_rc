<?php
namespace STALKER_CMS\Packages\Imagine\Gmagick;

use STALKER_CMS\Packages\Imagine\Effects\EffectsInterface;
use STALKER_CMS\Packages\Imagine\Exception\RuntimeException;
use STALKER_CMS\Packages\Imagine\Image\Palette\Color\ColorInterface;
use STALKER_CMS\Packages\Imagine\Exception\NotSupportedException;

/**
 * Effects implementation using the Gmagick PHP extension
 */
class Effects implements EffectsInterface {

    private $gmagick;

    public function __construct(\Gmagick $gmagick) {

        $this->gmagick = $gmagick;
    }

    /**
     * {@inheritdoc}
     */
    public function gamma($correction) {

        try {
            $this->gmagick->gammaimage($correction);
        } catch(\GmagickException $e) {
            throw new RuntimeException('Failed to apply gamma correction to the image');
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function negative() {

        if(!method_exists($this->gmagick, 'negateimage')) {
            throw new NotSupportedException('Gmagick version 1.1.0 RC3 is required for negative effect');
        }
        try {
            $this->gmagick->negateimage(FALSE, \Gmagick::CHANNEL_ALL);
        } catch(\GmagickException $e) {
            throw new RuntimeException('Failed to negate the image');
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function grayscale() {

        try {
            $this->gmagick->setImageType(2);
        } catch(\GmagickException $e) {
            throw new RuntimeException('Failed to grayscale the image');
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function colorize(ColorInterface $color) {

        throw new NotSupportedException('Gmagick does not support colorize');
    }

    /**
     * {@inheritdoc}
     */
    public function sharpen() {

        throw new NotSupportedException('Gmagick does not support sharpen yet');
    }

    /**
     * {@inheritdoc}
     */
    public function blur($sigma = 1) {

        try {
            $this->gmagick->blurImage(0, $sigma);
        } catch(\GmagickException $e) {
            throw new RuntimeException('Failed to blur the image', $e->getCode(), $e);
        }
        return $this;
    }
}
