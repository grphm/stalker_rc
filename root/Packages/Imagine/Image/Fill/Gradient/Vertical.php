<?php
namespace STALKER_CMS\Packages\Imagine\Image\Fill\Gradient;

use STALKER_CMS\Packages\Imagine\Image\PointInterface;

/**
 * Vertical gradient fill
 */
final class Vertical extends Linear {

    /**
     * {@inheritdoc}
     */
    public function getDistance(PointInterface $position) {

        return $position->getY();
    }
}
