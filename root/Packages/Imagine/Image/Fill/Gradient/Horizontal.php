<?php
namespace STALKER_CMS\Packages\Imagine\Image\Fill\Gradient;

use STALKER_CMS\Packages\Imagine\Image\PointInterface;

/**
 * Horizontal gradient fill
 */
final class Horizontal extends Linear {

    /**
     * {@inheritdoc}
     */
    public function getDistance(PointInterface $position) {

        return $position->getX();
    }
}
