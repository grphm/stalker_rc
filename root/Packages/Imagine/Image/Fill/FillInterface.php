<?php
namespace STALKER_CMS\Packages\Imagine\Image\Fill;

use STALKER_CMS\Packages\Imagine\Image\Palette\Color\ColorInterface;
use STALKER_CMS\Packages\Imagine\Image\PointInterface;

/**
 * Interface for the fill
 */
interface FillInterface {

    /**
     * Gets color of the fill for the given position
     *
     * @param PointInterface $position
     *
     * @return ColorInterface
     */
    public function getColor(PointInterface $position);
}
