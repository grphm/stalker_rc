<?php
namespace STALKER_CMS\Packages\Imagine\Filter\Advanced;

use STALKER_CMS\Packages\Imagine\Exception\InvalidArgumentException;
use STALKER_CMS\Packages\Imagine\Filter\FilterInterface;
use STALKER_CMS\Packages\Imagine\Image\ImageInterface;
use STALKER_CMS\Packages\Imagine\Image\Point;

/**
 * The OnPixelBased takes a callable, and for each pixel, this callable is called with the
 * image  (\Imagine\Image\ImageInterface) and the current point (\Imagine\Image\Point)
 */
class OnPixelBased implements FilterInterface {

    protected $callback;

    public function __construct($callback) {

        if(!is_callable($callback)) {
            throw new InvalidArgumentException('$callback has to be callable');
        }
        $this->callback = $callback;
    }

    /**
     * Applies scheduled transformation to ImageInterface instance
     * Returns processed ImageInterface instance
     *
     * @param ImageInterface $image
     *
     * @return ImageInterface
     */
    public function apply(ImageInterface $image) {

        $w = $image->getSize()->getWidth();
        $h = $image->getSize()->getHeight();
        for($x = 0; $x < $w; $x++) {
            for($y = 0; $y < $h; $y++) {
                call_user_func($this->callback, $image, new Point($x, $y));
            }
        }
        return $image;
    }
}
