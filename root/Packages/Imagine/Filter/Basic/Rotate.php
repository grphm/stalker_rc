<?php
namespace STALKER_CMS\Packages\Imagine\Filter\Basic;

use STALKER_CMS\Packages\Imagine\Image\ImageInterface;
use STALKER_CMS\Packages\Imagine\Image\Palette\Color\ColorInterface;
use STALKER_CMS\Packages\Imagine\Filter\FilterInterface;

/**
 * A rotate filter
 */
class Rotate implements FilterInterface {

    /**
     * @var integer
     */
    private $angle;
    /**
     * @var ColorInterface
     */
    private $background;

    /**
     * Constructs Rotate filter with given angle and background color
     *
     * @param integer $angle
     * @param ColorInterface $background
     */
    public function __construct($angle, ColorInterface $background = NULL) {

        $this->angle = $angle;
        $this->background = $background;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(ImageInterface $image) {

        return $image->rotate($this->angle, $this->background);
    }
}
