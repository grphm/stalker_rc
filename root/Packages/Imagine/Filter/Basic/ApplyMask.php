<?php
namespace STALKER_CMS\Packages\Imagine\Filter\Basic;

use STALKER_CMS\Packages\Imagine\Filter\FilterInterface;
use STALKER_CMS\Packages\Imagine\Image\ImageInterface;

/**
 * An apply mask filter
 */
class ApplyMask implements FilterInterface {

    /**
     * @var ImageInterface
     */
    private $mask;

    /**
     * @param ImageInterface $mask
     */
    public function __construct(ImageInterface $mask) {

        $this->mask = $mask;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(ImageInterface $image) {

        return $image->applyMask($this->mask);
    }
}
