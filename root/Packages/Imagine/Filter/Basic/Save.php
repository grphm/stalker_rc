<?php
namespace STALKER_CMS\Packages\Imagine\Filter\Basic;

use STALKER_CMS\Packages\Imagine\Image\ImageInterface;
use STALKER_CMS\Packages\Imagine\Filter\FilterInterface;

/**
 * A save filter
 */
class Save implements FilterInterface {

    /**
     * @var string
     */
    private $path;
    /**
     * @var array
     */
    private $options;

    /**
     * Constructs Save filter with given path and options
     *
     * @param string $path
     * @param array $options
     */
    public function __construct($path = NULL, array $options = array()) {

        $this->path = $path;
        $this->options = $options;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(ImageInterface $image) {

        return $image->save($this->path, $this->options);
    }
}
