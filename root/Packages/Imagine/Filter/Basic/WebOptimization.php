<?php
namespace STALKER_CMS\Packages\Imagine\Filter\Basic;

use STALKER_CMS\Packages\Imagine\Image\ImageInterface;
use STALKER_CMS\Packages\Imagine\Image\Palette\RGB;
use STALKER_CMS\Packages\Imagine\Filter\FilterInterface;

/**
 * A filter to render web-optimized images
 */
class WebOptimization implements FilterInterface {

    private $palette;
    private $path;
    private $options;

    public function __construct($path = NULL, array $options = array()) {

        $this->path = $path;
        $this->options = array_replace(array(
            'resolution-units' => ImageInterface::RESOLUTION_PIXELSPERINCH,
            'resolution-y' => 72,
            'resolution-x' => 72,
        ), $options);
        $this->palette = new RGB();
    }

    /**
     * {@inheritdoc}
     */
    public function apply(ImageInterface $image) {

        $image
            ->usePalette($this->palette)
            ->strip();
        if(is_callable($this->path)) {
            $path = call_user_func($this->path, $image);
        } elseif(NULL !== $this->path) {
            $path = $this->path;
        } else {
            return $image;
        }
        return $image->save($path, $this->options);
    }
}
