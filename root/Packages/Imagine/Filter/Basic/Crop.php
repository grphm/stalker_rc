<?php
namespace STALKER_CMS\Packages\Imagine\Filter\Basic;

use STALKER_CMS\Packages\Imagine\Image\ImageInterface;
use STALKER_CMS\Packages\Imagine\Image\BoxInterface;
use STALKER_CMS\Packages\Imagine\Image\PointInterface;
use STALKER_CMS\Packages\Imagine\Filter\FilterInterface;

/**
 * A crop filter
 */
class Crop implements FilterInterface {

    /**
     * @var PointInterface
     */
    private $start;
    /**
     * @var BoxInterface
     */
    private $size;

    /**
     * Constructs a Crop filter with given x, y, coordinates and crop width and
     * height values
     *
     * @param PointInterface $start
     * @param BoxInterface $size
     */
    public function __construct(PointInterface $start, BoxInterface $size) {

        $this->start = $start;
        $this->size = $size;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(ImageInterface $image) {

        return $image->crop($this->start, $this->size);
    }
}
