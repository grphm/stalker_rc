<?php
namespace STALKER_CMS\Packages\Imagine\Exception;

/**
 * Imagine-specific runtime exception
 */
class RuntimeException extends \RuntimeException implements Exception {

}
