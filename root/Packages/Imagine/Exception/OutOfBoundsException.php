<?php
namespace STALKER_CMS\Packages\Imagine\Exception;

/**
 * Imagine-specific out of bounds exception
 */
class OutOfBoundsException extends \OutOfBoundsException implements Exception {

}
