<?php
namespace STALKER_CMS\Packages\Imagine\Exception;

/**
 * Should be used when a driver does not support an operation.
 */
class NotSupportedException extends RuntimeException implements Exception {

}
