var elixir = require('laravel-elixir');
elixir(function (mix) {
    //LIGHT THEME
    mix.styles([
        "animate.min.css",
        "bootstrap-datetimepicker.min.css",
        "bootstrap-select.css",
        "lightgallery.min.css",
        "chosen.css",
        "dropzone.min.css",
        "font-awesome.min.css",
        "jquery.mCustomScrollbar.min.css",
        "material-design-iconic-font.min.css",
        "summernote.css",
        "sweetalert.css",
        "selectize.bootstrap3.css"
    ], 'public/core/light/css/vendor.css', 'public/src/vendors/light/css');
    mix.styles([
        "main.css",
        "main-extended.css",
    ], 'public/core/light/css/main.css', 'public/src/vendors/light/css');
    //DARK THEME
    mix.styles([
        "animate.min.css",
        "bootstrap-datetimepicker.min.css",
        "bootstrap-select.css",
        "lightgallery.min.css",
        "chosen.css",
        "dropzone.min.css",
        "font-awesome.min.css",
        "jquery.mCustomScrollbar.min.css",
        "material-design-iconic-font.min.css",
        "summernote.css",
        "sweetalert.css",
        "selectize.bootstrap3.css"
    ], 'public/core/dark/css/vendor.css', 'public/src/vendors/dark/css');
    mix.styles([
        "main.css",
        "main-extended.css",
    ], 'public/core/dark/css/main.css', 'public/src/vendors/dark/css');
});

elixir(function (mix) {
    //LIGHT THEME
    mix.scripts([
        "jquery.min.js",
        "moment.min.js",
        "moment-with-locales.min.js",
        "bootstrap.min.js",
        "jquery.mCustomScrollbar.concat.min.js",
        "jquery.nicescroll.min.js",
        "waves.min.js",
        "sweetalert.min.js",
        "chosen.jquery.js",
        "fileinput.min.js",
        "bootstrap-select.min.js",
        "bootstrap-datetimepicker.min.js",
        "dropzone.min.js",
        "summernote-updated.min.js",
        "jquery.validate.min.js",
        "jquery.form.js",
        "growl.min.js",
        "autosize.min.js",
        "jquery.mask.min.js",
        "lightgallery-all.min.js",
        "jquery.sortable.js",
        "ZeroClipboard.min.js",
        "jquery.selectize.min.js",
        "keymaster.js"
    ], 'public/core/light/js/vendor.js', 'public/src/vendors/light/js');
    mix.scripts([
        "functions.js",
        "validation_locales.js",
        "validation_rules.js",
        "main.js"
    ], 'public/core/light/js/main.js', 'public/src/js');
    //DARK THEME
    mix.scripts([
        "jquery.min.js",
        "moment.min.js",
        "moment-with-locales.min.js",
        "bootstrap.min.js",
        "jquery.mCustomScrollbar.concat.min.js",
        "jquery.nicescroll.min.js",
        "waves.min.js",
        "sweetalert.min.js",
        "chosen.jquery.js",
        "fileinput.min.js",
        "bootstrap-select.min.js",
        "bootstrap-datetimepicker.min.js",
        "dropzone.min.js",
        "summernote-updated.min.js",
        "jquery.validate.min.js",
        "jquery.form.js",
        "growl.min.js",
        "autosize.min.js",
        "jquery.mask.min.js",
        "lightgallery-all.min.js",
        "jquery.sortable.js",
        "ZeroClipboard.min.js",
        "jquery.selectize.min.js",
        "keymaster.js"
    ], 'public/core/dark/js/vendor.js', 'public/src/vendors/dark/js');
    mix.scripts([
        "functions.js",
        "validation_locales.js",
        "validation_rules.js",
        "main.js"
    ], 'public/core/dark/js/main.js', 'public/src/js');
});