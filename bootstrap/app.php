<?php
$app = new Illuminate\Foundation\Application(
    realpath(__DIR__ . '/../')
);
$app->singleton(
    Illuminate\Contracts\Http\Kernel::class,
    STALKER_CMS\Vendor\Http\Kernel::class
);
$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    STALKER_CMS\Vendor\Console\Kernel::class
);
$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    STALKER_CMS\Vendor\Exceptions\Handler::class
);
return $app;
